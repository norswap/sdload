package be_uclouvain_inl_sdload;

import be_uclouvain_inl_sdload.constraints.ChoiceConstraint;
import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.constraints.DictConstraint;
import be_uclouvain_inl_sdload.constraints.ListConstraint;
import be_uclouvain_inl_sdload.constraints.ValueConstraint;
import be_uclouvain_inl_sdload.constraints.WildcardConstraint;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTDict;
import be_uclouvain_inl_sdload.dlt.DLTList;
import be_uclouvain_inl_sdload.dlt.DLTValue;
import eu_norswap_utils.Caster;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 *
 */
public class TemplateGenerator
{
    public static final TemplateGenerator get = new TemplateGenerator();

    @FunctionalInterface
    private static interface GenFunction
    {
        DLT apply(Constraint constraint, DLT template, DLT parent);
    }

    private Map<Class<? extends Constraint>, GenFunction> switchTable =
        ImmutableMap.<Class<? extends Constraint>, GenFunction>
            builder()
            .put(DictConstraint.class,       this::generateFromDict)
            .put(ListConstraint.class,       this::generateFromList)
            .put(WildcardConstraint.class,   this::generateFromWildcard)
            .put(ValueConstraint.class,      this::generateFromValue)
            .put(ChoiceConstraint.class,     this::generateFromChoice)
            .build();

    public DLT generate(Constraint constraint, DLT template)
    {
        return generate(constraint, template, null);
    }

    public DLT generate(Constraint constraint, DLT template, DLT parent)
    {
        if (Holes.isHole(template)) {
            return constraint.generate(parent);
        }

        GenFunction func = switchTable.get(constraint.getClass());

        if (func != null) {
            return func.apply(constraint, template, parent);
        }

        return null;
    }

    public DLTDict generateFromDict(Constraint constraint, DLT template, DLT parent)
    {
        DictConstraint dc = Caster.cast(constraint);
        DLTDict in  = Caster.cast(template);
        DLTDict out = new DLTDict(in.size());

        for (Map.Entry<String, DLT> e : in.entrySet())
        {
            String key = e.getKey();

            Constraint c = dc.keyConstraints.get(key);
            if (c != null) {
                out.put(key, generate(c, e.getValue(), out));
                continue;
            }

            c = dc.optKeyConstraints.get(key);
            if (c != null) {
                out.put(key, generate(c, e.getValue(), out));
                continue;
            }

            out.put(key, e.getValue());
        }

        return out;
    }

    public DLTList generateFromList(Constraint constraint, DLT template, DLT parent)
    {
        ListConstraint  lc  = Caster.cast(constraint);
        DLTList         in  = Caster.cast(template);
        DLTList         out = new DLTList(in.size());

        for (DLT sub : in) {
            out.add(generate(lc.itemConstraint, sub, out));
        }

        return out;
    }

    @SuppressWarnings("unused")
    public DLTValue generateFromWildcard(Constraint constraint, DLT template, DLT parent)
    {
        return Caster.cast(template);
    }

    @SuppressWarnings("unused")
    public DLTValue generateFromValue(Constraint constraint, DLT template, DLT parent)
    {
        return Caster.cast(template);
    }

    public DLT generateFromChoice(Constraint constraint, DLT template, DLT parent)
    {
        ChoiceConstraint cc = Caster.cast(constraint);

        for (Constraint c : cc.alternatives())
        {
            if (c.check(template).satisfied()) {
                return generate(c, template, parent);
            }
        }

        throw GenerationFailedException.NO_CHOICE_SATISFYING_TEMPLATE(constraint.check(template));
    }
}
