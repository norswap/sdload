package be_uclouvain_inl_sdload.utils;

import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTList;
import be_uclouvain_inl_sdload.dlt.DLTValue;
import eu_norswap_utils.Numbers;
import eu_norswap_utils.Two;
import eu_norswap_utils.probabilities.Randomizer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

/**
 * Generates dependencies as Directed Acyclic Graph (DAG) edges.
 */
public class DependencyGenerator
{
    /**
     * The number of directed edges to generate.
     */
    public Supplier<Integer> nEdges;

    /**
     * The number of levels. The level of a node is the length of the maximum chain
     * starting at the node (since the graph is acyclic, the length is finite).
     */
    public Supplier<Integer> nLevels;

    /**
     * A function (lvl -> w), where (items.size() * (w / (sum w for all lvl))) is the
     * number of items at level lvl.
     */
    public Function<Integer, Double> levelToSizeWeight;

    /**
     * A function (lvl -> n), where n is the number of outgoing edges for each node at level lvl.
     */
    public Function<Integer, Integer> levelToNumDeps;

    /**
     * A function (src,dst -> w), where
     * levelToNumDeps(src) * (w / (sum interLevelWeight(src, dst) for all dst < src))
     * is the number of outgoing edges to nodes of level dst for each node at level src.
     */
    public BiFunction<Integer, Integer, Double> interLevelWeight;

    /**
     * Generates dependencies as Directed Acyclic Graph (DAG) edges. Returns a list of
     * (depender, dependee) pairs of items.
     *
     * Algorithm Preconditions:
     *
     * - The association of a the item count and the size weight function must result
     *   in each level being non-empty.
     *
     * - levelToNumDeps.apply(level) must return a number inferior or equal to the sum of the
     *   number of nodes in the higher levels (x < level).
     *
     */
    public <T> List<Two<T>> generateDeps(List<T> items)
    {
        List<Two<T>> out = new ArrayList<>();

        // Compute the level sizes and limits.

        int   nLevels     = this.nLevels.get();
        int[] levelSizes  = new int[nLevels];

        // levelLimits[i] = index one past the last item pertaining to level i in items.
        int[] levelLimits = new int[nLevels];

        double weightsSum =
            IntStream.range(0, nLevels).mapToDouble(levelToSizeWeight::apply).sum();

        for (int i = 0; i < nLevels; ++i)
        {
            double weight = levelToSizeWeight.apply(i);
            levelSizes[i] = (int) Math.round(weight / weightsSum * items.size());

            if (levelSizes[i] == 0)
            {
                throw new RuntimeException(String.format("Combination of item count and size " +
                    "weight function causes level %d to be empty (weight: %f).", i, weight));
            }

            levelLimits[i] = (i == 0 ? 0 : levelLimits[i - 1]) + levelSizes[i];
        }

        // For each level except the first, generate the dependencies of its items on the
        // items of the upper levels.

        for (int level = 1; level < nLevels; ++level)
        {
            int numDeps = levelToNumDeps.apply(level);

            if (levelLimits[level] < numDeps) {
                throw new RuntimeException("Requesting more dependencies than DAG ancestors.");
            }

            // Compute a list of potential dependency indices and their weights.

            List<Integer> indices = Numbers.range(0, levelLimits[level - 1]);
            List<Double>  weights = new ArrayList<>(indices.size());

            outer: for (int dst = 0 ; dst < nLevels ; ++dst)
            {
                double dstWeight = interLevelWeight.apply(level, dst);
                int start = dst == 0 ? 0 : levelLimits[dst - 1];

                for (int i = start ; i < levelLimits[dst] ; ++i)
                {
                    if (i >= indices.size()) {
                        break outer;
                    }

                    weights.add(dstWeight);
                }
            }

            // For each item at this level, generate its dependencies.

            for (int depender = levelLimits[level - 1]; depender < levelLimits[level]; ++depender)
            {
                List<Integer> dependees = Randomizer.randomOrder(indices, weights);

                // Mandatory dependency on an item of the above level.

                int dependee = Randomizer.between(
                    levelLimits[level - 1] - levelSizes[level - 1],
                    levelLimits[level - 1]);

                dependees.remove(new Integer(dependee));
                out.add(new Two<>(items.get(depender), items.get(dependee)));

                // Add remaining dependencies.

                for (int i = 1 ; i < numDeps ; ++i) {
                    out.add(new Two<>(items.get(depender), items.get(dependees.get(i - 1))));
                }
            }
        }

        return out;
    }

    /**
     * Same as {@link #generateDeps(java.util.List)}, but acts only on lists of DLT and returns
     * a DLTList where the pairs are also encoded as DLTLists.
     */
    public DLTList generateDLTDeps(List<DLT> items)
    {
        DLTList out = new DLTList();
        List<Two<DLT>> pairs = generateDeps(items);

        for (Two<DLT> pair : pairs)
        {
            DLTList dltPair = new DLTList();
            dltPair.add((DLT) pair.one.clone());
            dltPair.add((DLT) pair.two.clone());
            out.add(dltPair);
        }

        return out;
    }

    /**
     * Same as {@link #generateDeps(java.util.List)}, but acts only on lists of DLT and returns
     * a DLTList where the pairs are also encoded as DLTLists encoding the index of the initial
     * list.
     */
    public DLTList generateIndexDeps(List<DLT> items)
    {
        DLTList out = new DLTList();
        List<Two<Integer>> pairs = generateDeps(Numbers.range(0, items.size()));

        for (Two<Integer> pair : pairs)
        {
            DLTList dltPair = new DLTList();
            dltPair.add(new DLTValue(pair.one));
            dltPair.add(new DLTValue(pair.two));
            out.add(dltPair);
        }

        return out;
    }
}
