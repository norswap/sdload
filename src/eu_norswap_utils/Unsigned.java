package eu_norswap_utils;

/**
 *
 */
public class Unsigned
{
    public static int fromShort(short s)
    {
        return s & 0xffff;
    }

    public static short fromByte(byte b)
    {
        return (short) (b & 0xff);
    }

    public static long fromInt(int i)
    {
        return i & 0xffff_ffffl;
    }
}
