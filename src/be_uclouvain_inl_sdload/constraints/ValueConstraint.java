package be_uclouvain_inl_sdload.constraints;

import be_uclouvain_inl_sdload.CheckDiagnostic;
import be_uclouvain_inl_sdload.generators.DLTGenerator;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTValue;
import eu_norswap_utils.Caster;
import be_uclouvain_inl_sdload.utils.PathEnabled;

/**
 * Constraint for DLT wrapping a Java object.
 */
public class ValueConstraint extends AbstractConstraint
{
    public Object value;

    public ValueConstraint(Object value)
    {
        this.value = value;
        setGenerator(DLTGenerator.from(value));
    }

    @Override
    public CheckDiagnostic check(DLT dlt)
    {
        if (!(dlt instanceof DLTValue))  {
            return CheckDiagnostic.failed("not a DLT value");
        }

        if (!value.equals(Caster.<DLTValue>cast(dlt).value)) {
            return CheckDiagnostic.failed("wrong value");
        }

        return CheckDiagnostic.SATISFIED;
    }

    @Override
    public String toString()
    {
        return value.toString();
    }

    @Override
    public PathEnabled readNode(String key)
    {
        return null;
    }
}
