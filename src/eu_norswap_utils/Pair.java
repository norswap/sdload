package eu_norswap_utils;

/**
 * A pair of items.
 */
@SuppressWarnings("unchecked")
public class Pair<A, B> implements Cloneable
{
    public A one;
    public B two;

    public Pair(A one, B two)
    {
        this.one = one;
        this.two = two;
    }

    @Override
    public String toString()
    {
        return "(" + one + "," + two + ")";
    }

    @Override
    public Object clone()
    {
        return new Two(one, two);
    }
}