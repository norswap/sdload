package be_uclouvain_inl_sdload.constraints;

import be_uclouvain_inl_sdload.generators.DLTGenerator;

import java.util.HashMap;
import java.util.Map;

/**
 * Supplies default implementations for DLT-related methods of the {@link Constraint} interface.
 */
public abstract class AbstractConstraint implements Constraint
{
    private DLTGenerator generator;
    private Map<String, Object> properties;

    @Override
    public DLTGenerator generator()
    {
        return generator;
    }

    @Override
    public void setGenerator(DLTGenerator generator)
    {
        this.generator = generator;
    }

    @Override
    public Map<String, Object> properties()
    {
        if (properties == null) {
            this.properties = new HashMap<>();
        }

        return properties;
    }
}
