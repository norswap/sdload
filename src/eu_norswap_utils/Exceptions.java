package eu_norswap_utils;

/**
 * Misc. utilities to work with exceptions.
 */
public class Exceptions
{
    /**
     * Like {@link java.util.function.Supplier}, but allows throwing checked exceptions from
     * the get method.
     */
    public static interface ThrowableSupplier<T>
    {
        T get() throws Exception;
    }

    /**
     * Like {@link java.lang.Runnable}, but allows throwing checked exceptions from
     * the run method.
     */
    public static interface ThrowableRunnable
    {
        void run() throws Exception;
    }

    /**
     * Returns {@code supplier.get()}, re-throwing any exception thrown as a run-time exception.
     */
    public static <T> T dyn(ThrowableSupplier<T> supplier)
    {
        try {
            return supplier.get();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Runs {@code runnable.run()}, re-throwing any exception thrown as a run-time exception.
     */
    public static void dyn(ThrowableRunnable runnable)
    {
        try {
            runnable.run();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Runs {@code runnable.run()}, swallowing any thrown exception.
     */
    public static void swallow(ThrowableRunnable runnable)
    {
        try {
            runnable.run();
        }
        catch (Exception e) {}
    }
}
