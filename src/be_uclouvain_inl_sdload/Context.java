package be_uclouvain_inl_sdload;

/**
 * Records context that may be used by generators.
 */
public class Context
{
    private static Context instance = new Context();

    public static Context get()
    {
        return instance;
    }

    /**
     * Switch number to which a command is being sent. Meaningless when not generating
     * a command.
     */
    public int destination;
}
