package be_uclouvain_inl_sdload.dlt;

import eu_norswap_utils.Caster;

import java.util.LinkedHashMap;

/**
 * A dictionary whose key-value pairs are ordered in insertion-order, implementing the DLT
 * interface.
 */
public class DLTDict extends LinkedHashMap<String, DLT> implements DLT
{
    private DLT parent;
    private DLTPath.Component parentComponent;

    /**
     * Construct a new DLTDict with some initial size.
     */
    public DLTDict(int size)
    {
        super(size);
    }

    /**
     * Construct a new DLTDict with the default initial size.
     */
    public DLTDict() {}

    @Override
    public DLT parent()
    {
        return parent;
    }

    @Override
    public DLTPath.Component parentComponent() { return parentComponent; }

    @Override
    public void setParent(DLT parent, DLTPath.Component parentComponent)
    {
        this.parent = parent;
        this.parentComponent = parentComponent;
    }

    @Override
    public Object getAtPath(DLTPath path)
    {
        if (path.isEmpty())
        {
            return this;
        }

        if (path.head() instanceof DLTPath.Parent)
        {
            return parent.getAtPath(path.tail());
        }

        if (path.head() instanceof DLTPath.Key)
        {
            String key = Caster.<DLTPath.Key>cast(path.head()).key;
            DLT sub = get(key);

            if (sub != null) {
                return sub.getAtPath(path.tail());
            }

            throw new RuntimeException("DLT dictionary doesn't have key: " + key
                    + " (dict: " + this + ")");
        }

        throw new RuntimeException("Wrong path component for DLT dictionary: "
                + path.head().getClass() + " (dict: " + this + ")");
    }

    @Override
    public DLT put(String key, DLT value)
    {
        value.setParent(this, new DLTPath.Key(key));
        return super.put(key, value);
    }

    /**
     * Put an key-value pair into the dictionary, after converting the value into a DLT (see
     * {@link DLTs#from(Object)} for the details of the conversion).
     */
    public DLT putObject(String key, Object value)
    {
        DLT dlt = DLTs.from(value);
        return put(key, dlt);
    }

    @Override
    public DLTDict clone()
    {
        DLTDict out = Caster.cast(super.clone());
        out.parent = parent;
        return out;
    }
}
