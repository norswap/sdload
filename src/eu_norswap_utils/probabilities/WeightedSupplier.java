package eu_norswap_utils.probabilities;

import eu_norswap_utils.vendor.AliasMethod;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

/**
 * A weighted supplier supplies elements from a set, with probabilities adjusted according to a
 * corresponding set of weights.
 */
public class WeightedSupplier<T> implements Supplier<T>
{
    private final AliasMethod alias;
    public  final ImmutableList<T> elements;

    private WeightedSupplier(ImmutableList<T> elements, AliasMethod alias)
    {
        this.alias    = alias;
        this.elements = elements;
    }

    public static <T> WeightedSupplier<T> create(List<T> elements, List<Double> weights)
    {
        assert elements.size() == weights.size();

        return new WeightedSupplier<>(
                ImmutableList.copyOf(elements),
                new AliasMethod(Weighted.normalize(weights)));
    }

    public static <T extends Weighted> WeightedSupplier<T> create(Collection<T> elements)
    {
        ImmutableList<T> list = ImmutableList.copyOf(elements);

        List<Double> weights = new ArrayList<>(list.size());
        for (T t : list) {
            weights.add(t.weight());
        }
        Weighted.normalizeInPlace(weights);

        return new WeightedSupplier<>(list, new AliasMethod(weights));
    }

    @Override
    public T get()
    {
        return elements.get(alias.next());
    }
}