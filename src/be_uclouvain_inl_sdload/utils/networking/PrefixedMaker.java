package be_uclouvain_inl_sdload.utils.networking;

import com.google.common.collect.ObjectArrays;

/**
 * Build string representation of things which are chains of number (such as IP and MAC addresses).
 * Allows to pre-supply a prefix of the chain. String representation can then be constructed
 * by only passing the suffix.
 */
public abstract class PrefixedMaker
{
    public Integer[] prefix;

    /**
     * Size of the chains to represent. {@code size == prefix.length + suffix.length}
     */
    public int size;

    /**
     * 1 + the maximal allowable value for each number in the chain.
     * e.g. 256 if numbers are between 0 and 255 inclusive.
     */
    public int maxValue;

    /**
     * The format string used to display the chain of number. The {@link String#format} parameters
     * are numbers in the chain (as {@link Integer}).
     */
    public String format;

    /**
     * The name of the thing we generate a string representation of, prefixed by an article.
     * e.g. "an IPv4 address"
     */
    public String thing;

    public PrefixedMaker(String thing, int size, int maxValue, String format, Integer... prefix)
    {
        if (prefix.length > 4) {
            throw new RuntimeException(String.format(
                    "Trying to build %s maker with more than %d components in the prefix.",
                    thing, size));
        }

        this.thing = thing;
        this.size = size;
        this.maxValue = maxValue;
        this.format = format;
        this.prefix = prefix;
    }

    public String make(Integer... suffix)
    {
        if (prefix.length + suffix.length != size) {
            throw new RuntimeException(String.format(
                    "Trying to build %s that has not exactly %d components.", thing, size));
        }

        return String.format(format, (Object[]) ObjectArrays.concat(prefix, suffix, Integer.class));
    }

    /**
     * Build a string representation by using a suffix formed by iteratively taking the remainder
     * of value by maxValue, then dividing by maxValue. The first remainder is the last number
     * in the suffix, etc.
     */
    public String make(long value)
    {
        int suffixLength = size - prefix.length;
        Integer[] suffix = new Integer[suffixLength];

        for (int i = suffixLength - 1 ; i >= 0 ; --i)
        {
            suffix[i] = (int) (value % maxValue);
            value = (int) (value / maxValue);
        }

        if (value > 0) {
            throw new RuntimeException(
                "Value too large w.r.t. the suffix size for this " + thing + ".");
        }



        return make(suffix);
    }

    /**
     * Builds String representations of IPv4 addresses.
     */
    public static class IPv4 extends PrefixedMaker
    {
        public IPv4(Integer... prefix)
        {
            super("an IPv4 address", 4, 256, "%d.%d.%d.%d", prefix);
        }
    }

    /**
     * Builds String representations of MAC addresses.
     */
    public static class MAC extends PrefixedMaker
    {
        public MAC(Integer... prefix)
        {
            super("a MAC address", 6, 256, "%02X:%02X:%02X:%02X:%02X:%02X", prefix);
        }
    }
}


