package eu_norswap_utils;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Utilities to work on lists in the reverse order. In general those should only be used on lists
 * that support efficient iteration in the reverse order via {@link java.util.List#listIterator()}.
 */
public class Reverse
{
    /**
     * Calls {@code consumer} on all elements of the list, in reverse order.
     * {@code Reverse.forEach(list, consumer)} is more efficient than
     * {@code Reverse.stream(list).forEach(consumer)}, but works on random access lists.
     */
    public static <E, L extends List<E> & RandomAccess>
    void forEach(L list, Consumer<? super E> consumer)
    {
        for (int i = list.size() - 1 ; i >= 0 ; --i) {
            consumer.accept(list.get(i));
        }
    }

    /**
     * A class that wraps another list iterator, inverting its iteration order.
     */
    public static class ReverseListIterator<E> implements ListIterator<E>
    {
        public final ListIterator<E> forward;

        private ReverseListIterator(ListIterator<E> forward)
        {
            this.forward = forward;
        }

        @Override
        public boolean hasNext()
        {
            return forward.hasPrevious();
        }

        @Override
        public E next()
        {
            return forward.previous();
        }

        @Override
        public boolean hasPrevious()
        {
            return forward.hasNext();
        }

        @Override
        public E previous()
        {
            return forward.next();
        }

        @Override
        public int nextIndex()
        {
            return forward.previousIndex();
        }

        @Override
        public int previousIndex()
        {
            return forward.nextIndex();
        }

        @Override
        public void remove()
        {
            forward.remove();
        }

        @Override
        public void set(E e)
        {
            forward.set(e);
        }

        @Override
        public void add(E e)
        {
            forward.add(e);
        }
    }

    /**
     * Return an iterator which is the reverse of the supplied iterator, by wrapping it
     * in a {@link ReverseListIterator}, unless the iterator is already a
     * {@link ReverseListIterator}, in which case the initial iterator (the one that was
     * reverse to obtain {@code forwardIterator}) is returned.
     */
    public static <E> ListIterator<E> iterator(ListIterator<E> forwardIterator)
    {
        return forwardIterator instanceof ReverseListIterator
            ? ((ReverseListIterator<E>) forwardIterator).forward
            : new ReverseListIterator<>(forwardIterator);
    }

    /**
     * Return an iterator on the elements of the list, in reverse order.
     */
    public static <E> Iterator<E> iterator(List<E> list)
    {
        return iterator(list.listIterator());
    }

    /**
     * Return an iterable backed by the reverse iterator of {@code forwardIterator}.
     * See {@link #iterator(java.util.ListIterator)}.
     */
    public static <E> Iterable<E> iterable(ListIterator<E> forwardIterator)
    {
        return new Iterable<E>()
        {
            @Override
            public Iterator<E> iterator()
            {
                return Reverse.iterator(forwardIterator);
            }
        };
    }

    /**
     * Return an iterable over the elements of the list, in reverse order.
     * See {@link #iterable(java.util.ListIterator)}.
     */
    public static <E> Iterable<E> iterable(List<E> list)
    {
        return iterable(list.listIterator());
    }

    /**
     * Return a finite non-concurrent stream with the elements of the list, in reverse order.
     */
    public static <E> Stream<E> stream(List<E> list)
    {
        return StreamSupport.stream(
            Spliterators.spliterator(iterator(list), list.size(), Spliterator.ORDERED),
            false);
    }
}
