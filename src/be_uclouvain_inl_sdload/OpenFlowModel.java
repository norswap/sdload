package be_uclouvain_inl_sdload;

import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.constraints.DictConstraint;
import be_uclouvain_inl_sdload.constraints.ListConstraint;
import be_uclouvain_inl_sdload.constraints.WeightedChoiceConstraint;
import be_uclouvain_inl_sdload.dlt.DLTDict;
import be_uclouvain_inl_sdload.generators.SubDictionaryGenerator;
import be_uclouvain_inl_sdload.generators.networking.SwitchGenerator;
import be_uclouvain_inl_sdload.generators.networking.TopoPortGenerator;
import be_uclouvain_inl_sdload.utils.ParameterHolder;
import be_uclouvain_inl_sdload.utils.networking.Switches;
import com.beust.jcommander.Parameter;
import eu_norswap_utils.probabilities.Randomizer;

import java.util.function.Supplier;

import static be_uclouvain_inl_sdload.DSL.*;

/**
 * The {@code top} field hold the constraint after which to generate the output if the user
 * doesn't supply another model. Parts of the model can be reused in other models, and the model
 * can be modified.
 */
public class OpenFlowModel implements ApplicationModel
{
    // ---------------------------------------------------------------------------------------------
    // exposed constraints

    /**
     * Constraint for a message that send an OpenFlow command to a switch.
     */
    public DictConstraint message;

    /**
     * Constraint for a group (list) of OpenFlow messages.
     */
    public ListConstraint messageGroup;

    /**
     * Constraint for a list of groups.
     */
    public ListConstraint groupList;

    /**
     * Constraint for an OpenFlow command.
     */
    public WeightedChoiceConstraint command;

    /**
     * Constraint for an OpenFlow action (forward, drop, modify-field, ...).
     */
    public WeightedChoiceConstraint action;

    /**
     * A map from all OpenFlow matching criteria supported by SDLoad (header fields,
     * ingress port, ...) to their value for a packet.
     */
    public DictConstraint matchingCriteria;

    /**
     * A subset of the matching criteria for a packet, and the associated values.
     */
    public DictConstraint matchingCriteriaSubset;

    /**
     * Constraint for a (partial) set of header fields that can be modified via the OpenFlow
     * modify-field command, and the values to set those fields to.
     */
    public DictConstraint headerFields;

    /**
     * Constraint for an ingress port of the switch to which an OpenFlow message is being sent.
     * Only use a sub-constraint for an OpenFlow message.
     */
    public Constraint ingressPort;

    // ---------------------------------------------------------------------------------------------
    // options

    public static class Options implements ParameterHolder
    {
        /**
         * The number of groups of OpenFlow messages to generation.
         */
        @Parameter(names = "-nGroups")
        public int nGroups = 1;

        /**
         * Minimum number of messages per groups.
         */
        @Parameter(names = "-messagesPerGroupMin")
        public int messagesPerGroupMin = 100;

        /**
         * Maximum number of messages per groups.
         */
        @Parameter(names = "-messagesPerGroupMax")
        public int messagesPerGroupMax = 100;

        /**
         * The minimum number of fields to generate for matching packets and for modifying header
         * fields.
         */
        @Parameter(names = "-fieldsMin")
        public int fieldsMin = 1;

        /**
         * The maximum number of fields to generate for matching packets and for modifying header
         * fields.
         */
        @Parameter(names = "-fieldsMax")
        public int fieldsMax = 1;

        /**
         * A supplier of the number of fields to generate for matching packets and for
         * modifying header fields, synthesized from {@link #fieldsMin} and {@link #fieldsMax}.
         */
        public Supplier<Number> nFields;

        @Override
        public void configure()
        {
            this.nFields = () -> Randomizer.betweenClosed(fieldsMin, fieldsMax);
        }
    }

    public static final Options OPTIONS = new Options();

    // ---------------------------------------------------------------------------------------------
    // model

    @Override
    public Constraint constraint()
    {
        return groupList;
    }

    @Override
    public ParameterHolder options()
    {
        return OPTIONS;
    }

    // ---------------------------------------------------------------------------------------------
    // constraints definitions

    @Override
    public void buildConstraint()
    {
        Constraint ingress_port = Settings.get().topology() == null
                ? integer(1, Settings.get().nPortsPerSwitch).get()
                : integer().generator(TopoPortGenerator.get).get();

        headerFields =
        dict()
            .opt("ether_src",   SwitchGenerator.MACConstraint)
            .opt("ether_dst",   SwitchGenerator.MACConstraint)
            .opt("vlan_id",     integer(0, 1024))
            .opt("vlan_prio",   integer(0, 7))
            .opt("ip_src",      SwitchGenerator.IPConstraint)
            .opt("ip_dst",      SwitchGenerator.IPConstraint)
            .opt("tp_src_port", integer(0, Short.MAX_VALUE))
            .opt("tp_dst_port", integer(0, Short.MAX_VALUE))
        .generatorUsingSelf(self -> new SubDictionaryGenerator(
            OPTIONS.nFields, self.optKeyConstraints))
        .get();

        this.matchingCriteria =
        dict()
            .perform(b -> b.get().keyConstraints.putAll(headerFields.optKeyConstraints))
            .key("ingress_port", ingress_port)
        .get();

        this.matchingCriteriaSubset =
        dict()
            .perform(b -> b.get().optKeyConstraints.putAll(matchingCriteria.keyConstraints))
        .generatorUsingSelf(self -> new SubDictionaryGenerator(
            OPTIONS.nFields, self.optKeyConstraints))
        .get();

        this.action =
        choice()
            .or(0.8, dict()
                .key("type", value("forward"))
                .key("port", ingress_port))
            .or(0.2, dict()
                .key("type", value("mod_field"))
                .key("set", headerFields))
            .or(0.1, dict()
                .key("type", value("flood")))
            .or(0.1, dict()
                .key("type", value("drop")))
        .commit()
        .get();

        this.command =
        choice()
            .or(0.6, dict()
                .key("type", value("add_rule"))
                .key("action", action)
                .key("match", matchingCriteriaSubset))
            .or(0.2, dict()
                .key("type", value("del_rule"))
                .key("match", matchingCriteriaSubset))
            .or(0.0, dict()
                .key("type", value("mod_rule"))
                .key("action", action)
                .key("match", matchingCriteriaSubset))
            .or(0.2, dict()
                .key("type", value("query_switch")))
        .commit()
        .get();

        this.message = dict()
        .key("destination", SwitchGenerator.IPConstraint)
        .key("command", command)
            // TODO ugly
        .doBeforeGenerating(() -> {
            Context.get().destination = SwitchGenerator.get.get();
        })
        .extendGenerator(out -> {
            ((DLTDict) out).put("destination",
                dltValue(Switches.IPV4_MAKER.make(Context.get().destination)));
            return out;
        })
        .get();

        this.messageGroup = list(message).listGenerator(
            OPTIONS.messagesPerGroupMin,
            OPTIONS.messagesPerGroupMax).get();

        this.groupList = list(messageGroup).listGenerator(OPTIONS.nGroups).get();
    }

    // ---------------------------------------------------------------------------------------------
    // main

    public static void main(String[] args)
    {
        OPTIONS.fillWithCmdLineParameters(args);
        Settings.get().fillWithCmdLineParameters(args);
        SDLoad.run(new OpenFlowModel());
    }
}
