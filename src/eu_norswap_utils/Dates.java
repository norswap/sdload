package eu_norswap_utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Misc. utilities to work with dates and times.
 */
public class Dates
{
    public static final DateFormat ISO8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");

    public static String toISO8601(Date date, TimeZone timezone)
    {
        DateFormat df = (DateFormat) ISO8601Format.clone();
        df.setTimeZone(timezone);
        return df.format(new Date());
    }

    public static String toISO8601(Date date, String timezone)
    {
        return toISO8601(date, TimeZone.getTimeZone(timezone));
    }
}
