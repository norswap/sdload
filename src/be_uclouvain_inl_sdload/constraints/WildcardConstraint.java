package be_uclouvain_inl_sdload.constraints;

import be_uclouvain_inl_sdload.CheckDiagnostic;
import be_uclouvain_inl_sdload.generators.DLTGenerator;
import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTValue;
import eu_norswap_utils.Caster;
import be_uclouvain_inl_sdload.utils.PathEnabled;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;

/**
 * A constraint that checks if the DLT value is any value of a specified type (i.e. an instance
 * of a particular Java class). Also support intervals for Long and Double types.
 *
 * The class maintains a static map from classes to default generator for those classes.
 */
public class WildcardConstraint extends AbstractConstraint
{
    private static final Map<Class<?>, DLTGenerator> generators = new HashMap<>();
    static {
        generators.put(String.class,
                DLTGenerator.from(UUID.randomUUID().toString()));
        generators.put(Boolean.class,
                DLTGenerator.from(Randomizer.random().nextBoolean()));
        generators.put(Long.class,
                DLTGenerator.from(Randomizer.random().nextLong()));
        generators.put(Double.class,
                DLTGenerator.from(Randomizer.random().nextDouble() * Long.MAX_VALUE));
    }

    public static <T> void registerGenerator(Class<T> klass, Supplier<T> supplier)
    {
        generators.put(klass, DLTGenerator.from(supplier));
    }

    public static void removeGenerator(Class<?> klass)
    {
        generators.remove(klass);
    }

    public final Class<?> type;

    public WildcardConstraint(Class<?> type)
    {
        this.type = type;
        setGenerator(generators.get(type));
    }

    @Override
    public CheckDiagnostic check(DLT dlt)
    {
        if (!(dlt instanceof DLTValue)) {
            return CheckDiagnostic.failed("not a DLT value");
        }

        DLTValue value = Caster.cast(dlt);

        return type.isAssignableFrom(value.value.getClass())
            ? CheckDiagnostic.SATISFIED
            : CheckDiagnostic.failed("wrong type");
    }

    @Override
    public String toString()
    {
        return type.getCanonicalName();
    }

    @Override
    public PathEnabled readNode(String key)
    {
        return null;
    }
}
