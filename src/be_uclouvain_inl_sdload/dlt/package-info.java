/**
 * Contains the {@link be_uclouvain_inl_sdload.dlt.DLT} interface and implementations
 * thereof, as well as associated utilities ({@link be_uclouvain_inl_sdload.dlt.DLTs},
 * {@link be_uclouvain_inl_sdload.dlt.DLTPath}).
 */
package be_uclouvain_inl_sdload.dlt;