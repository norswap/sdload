package be_uclouvain_inl_sdload.examples;

import be_uclouvain_inl_sdload.ApplicationModel;
import be_uclouvain_inl_sdload.OpenFlowModel;
import be_uclouvain_inl_sdload.SDLoad;
import be_uclouvain_inl_sdload.Settings;
import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.generators.networking.SwitchGenerator;
import be_uclouvain_inl_sdload.utils.ParameterHolder;
import com.beust.jcommander.Parameter;

import static be_uclouvain_inl_sdload.DSL.anyOf;
import static be_uclouvain_inl_sdload.DSL.dict;
import static be_uclouvain_inl_sdload.DSL.list;
import static be_uclouvain_inl_sdload.DSL.value;

/**
 * A very simple model that represents a stream of basic network events: switchs going up and down
 * and OpenFlow packet-ins.
 */
public class ReactiveModel implements ApplicationModel
{
    public Constraint constraint;

    // ---------------------------------------------------------------------------------------------
    // options

    public static class Options implements ParameterHolder
    {
        /**
         * Number of events to generate.
         */
        @Parameter(names = "-nEvents")
        public int nEvents = 10;
    }

    public static final Options OPTIONS = new Options();

    // ---------------------------------------------------------------------------------------------
    // model

    @Override
    public Constraint constraint()
    {
        return constraint;
    }

    @Override
    public ParameterHolder options()
    {
        return OPTIONS;
    }

    // ---------------------------------------------------------------------------------------------
    // constraint definition

    @Override
    public void buildConstraint()
    {
        OpenFlowModel openFlowModel = new OpenFlowModel();
        openFlowModel.options().configure();
        openFlowModel.buildConstraint();

        constraint = list(anyOf(
            dict()
                .key("type",    anyOf(
                                    value("switch_up"),
                                    value("switch_down")))
                .key("switch",  SwitchGenerator.constraint),
            dict()
                .key("type",    value("packet_in"))
                .key("from",    SwitchGenerator.constraint)
                .key("header",  openFlowModel.matchingCriteria)
        )).listGenerator(OPTIONS.nEvents).get();
    }

    // ---------------------------------------------------------------------------------------------
    // main

    public static void main(String[] args)
    {
        OPTIONS.fillWithCmdLineParameters(args);
        Settings.get().fillWithCmdLineParameters(args);
        SDLoad.run(new ReactiveModel());
    }
}
