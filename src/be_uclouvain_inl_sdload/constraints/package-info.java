/**
 * Contains the {@link be_uclouvain_inl_sdload.constraints.Constraint} interface and implementations
 * thereof. A constraint is a predicate that a DLT of interest should satisfy. It may also
 * be associated to a {@link be_uclouvain_inl_sdload.generators.DLTGenerator} able to generate DLTs
 * that satisfy the constraint.
 */
package be_uclouvain_inl_sdload.constraints;