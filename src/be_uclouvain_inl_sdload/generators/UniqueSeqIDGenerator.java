package be_uclouvain_inl_sdload.generators;

import be_uclouvain_inl_sdload.constraints.Constraint;

import java.util.function.Supplier;

/**
 * A generator of unique sequential IDs. IDs are only unique relative to the other IDs generated
 * by the same generator.
 */
public class UniqueSeqIDGenerator implements Supplier<Integer>
{
    int next = 0;

    @Override
    public Integer get()
    {
        return next++;
    }
}
