package be_uclouvain_inl_sdload.generators.networking;

import be_uclouvain_inl_sdload.Context;
import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.Settings;
import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.constraints.IntegerRangeConstraint;
import be_uclouvain_inl_sdload.constraints.WildcardConstraint;
import be_uclouvain_inl_sdload.utils.networking.Switches;

import java.util.function.Supplier;

/**
 * A generator of switch IDs: an integer from 0 (inclusive) to the switch count (exclusive).
 * Only works with up to 65535 (2^16-1) switches.
 */
public class SwitchGenerator implements Supplier<Integer>
{
    public static final SwitchGenerator get   = new SwitchGenerator();
    public static final Constraint constraint =
        new IntegerRangeConstraint(0, Settings.get().nSwitches - 1);

    public static final Constraint IPConstraint  = new WildcardConstraint(String.class);
    public static final Constraint MACConstraint = new WildcardConstraint(String.class);

    public static final IP IPGen    = new IP();
    public static final MAC MACGen  = new MAC();

    static {
        constraint.setGenerator(get);
        IPConstraint .setGenerator(IPGen);
        MACConstraint.setGenerator(MACGen);
    }

    @Override
    public Integer get()
    {
        return Randomizer.nextInt(Math.min(Settings.get().nSwitches, 65535));
    }

    public static class IP implements Supplier<String>
    {
        @Override
        public String get()
        {
            int switchNum = SwitchGenerator.get.get();
            return Switches.IPV4_MAKER.make(switchNum);
        }
    }

    public static class MAC implements Supplier<String>
    {
        @Override
        public String get()
        {
            return Switches.MAC_MAKER.make(SwitchGenerator.get.get());
        }
    }
}
