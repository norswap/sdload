package be_uclouvain_inl_sdload.constraints;

import be_uclouvain_inl_sdload.CheckDiagnostic;
import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTValue;
import be_uclouvain_inl_sdload.utils.PathEnabled;

/**
 * Verifies that the DLT is a number comprised between two floating-point bounds. The
 * maximum bounds are the Double type bounds (+/- Infinity), which are also the default bounds.
 */
public class FloatRangeConstraint extends AbstractConstraint
{
    public double min;
    public double max;
    public boolean checkBounds;

    public FloatRangeConstraint()
    {
        this.checkBounds = false;
        setGenerator(parent -> new DLTValue(Randomizer.random().nextDouble() * Long.MAX_VALUE));
    }

    public FloatRangeConstraint(double min, double max)
    {
        this.min = min;
        this.max = max;
        setGenerator(parent -> new DLTValue(Randomizer.between(min, max)));
    }

    @Override
    public CheckDiagnostic check(DLT dlt)
    {
        if (!(dlt instanceof DLTValue)) {
            return CheckDiagnostic.failed("not a DLT value");
        }

        DLTValue dltValue = (DLTValue) dlt;

        if (!(dltValue.value instanceof Number)) {
            return CheckDiagnostic.failed("not a number");
        }

        double floating  = ((Number) dltValue.value).doubleValue();

        if (checkBounds && !(floating >= min && floating <= max)) {
            return CheckDiagnostic.failed("float out of range");
        }

        return CheckDiagnostic.SATISFIED;
    }

    @Override
    public String toString()
    {
        return String.format(" [%f - %f]", min, max);
    }

    @Override
    public PathEnabled readNode(String key)
    {
        return null;
    }
}