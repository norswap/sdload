package be_uclouvain_inl_sdload;

/**
 *
 */
public class GenerationFailedException extends RuntimeException
{
    public static enum Type
    {
        NO_CHOICE_SATISFYING_TEMPLATE
    }

    public Type type;
    public CheckDiagnostic diagnostic;

    public static GenerationFailedException
    NO_CHOICE_SATISFYING_TEMPLATE(CheckDiagnostic diagnostic)
    {
        GenerationFailedException e = new GenerationFailedException();
        e.type = Type.NO_CHOICE_SATISFYING_TEMPLATE;
        e.diagnostic = diagnostic;
        return e;
    }
}
