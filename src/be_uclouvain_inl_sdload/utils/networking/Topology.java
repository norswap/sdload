package be_uclouvain_inl_sdload.utils.networking;

import be_uclouvain_inl_sdload.utils.Identifiable;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Table;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.graph.UndirectedGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a topology: a bunch of switches connected by (bidirectional) links.
 *
 * - Switches are numbered from 0.
 * - Ports are numbered from 1.
 */
public class Topology
{
    public static class Switch extends Identifiable<Switch>
    {
        public Switch(int id)
        {
            super(id);
        }

        @Override
        public String toString()
        {
            return "v" + super.toString();
        }
    }

    public static class Link extends Identifiable<Link>
    {
        public Link(int id)
        {
            super(id);
        }

        @Override
        public String toString()
        {
            return "e" + super.toString();
        }
    }

    /**
     * Map from link id to link weight.
     */
    public Map<Integer, Integer> weights = new HashMap<>();

    private UndirectedGraph<Switch, Link> graph;
    private DijkstraShortestPath<Switch, Link> shortestPaths;
    private Table<Switch, Integer, Switch> ports;
    private Map<Integer, Switch> switches;

    public Topology(UndirectedGraph<Switch, Link> graph)
    {
        this.graph = graph;

        // Assign neighboring switches to port numbers.

        this.ports = HashBasedTable.create();
        for (Switch v : graph.getVertices())
        {
            int i = 1;
            for (Switch w : graph.getNeighbors(v)) {
                ports.put(v, i++, w);
            }
        }

        for (Link l : graph.getEdges()) {
            weights.put(l.id, 1);
        }
        reloadLinkWeights();

        // Make a mapping from switch id to switches.

        ImmutableMap.Builder<Integer, Switch> builder = ImmutableMap.builder();
        graph.getVertices().stream().forEach(v -> builder.put(v.id, v));
        this.switches = builder.build();
    }

    public void reloadLinkWeights()
    {
        this.shortestPaths = new DijkstraShortestPath<>(graph, link -> weights.get(link.id), false);
    }

    public UndirectedGraph<Switch, Link> graph()
    {
        return graph;
    }

    public Switch switchWith(int id)
    {
        return switches.get(id);
    }

    public Switch onPort(Switch aSwitch, int portNumber)
    {
        return ports.get(aSwitch, portNumber);
    }

    public int portTowards(Switch source, Switch destination)
    {
        for (Map.Entry<Integer, Switch> e : ports.row(source).entrySet())
        {
            if (e.getValue().equals(destination)) {
                return e.getKey();
            }
        }

        return -1;
    }

    public List<Switch> shortestPath(Switch from, Switch to)
    {
        return shortestPaths.getPath(from, to).stream().collect(
                () -> new ArrayList<>(ImmutableList.of(from)),
                (list, edge) -> list.add(graph.getOpposite(list.get(list.size()-1), edge)),
                ArrayList::addAll);
    }

    public List<Switch> shortestPath(int from, int to)
    {
        return shortestPath(new Switch(from), new Switch(to));
    }
}
