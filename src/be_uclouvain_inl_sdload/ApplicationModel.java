package be_uclouvain_inl_sdload;

import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.utils.ParameterHolder;

/**
 * Holds all info needed to generate input for a given application (or module, component, protocol
 * ... whatever).
 *
 * See the "examples" package for concrete instantiation of this interface.
 */
public interface ApplicationModel
{
    /**
     * The top-level constraint used to generate the input. Undefined before
     * {@link #buildConstraint()} is called.
     */
    Constraint constraint();

    /**
     * The application-specific options. See {@link be_uclouvain_inl_sdload.utils.ParameterHolder}.
     * The settings that are relevant to all applications are stored separately, in
     * {@link be_uclouvain_inl_sdload.Settings}.
     */
    ParameterHolder options();

    /**
     * Dynamically construct the constraint (if needed) using settings and options.
     * If the settings or options changed and one wish the reflect those changes, this method
     * must be called again. Hence, implementers should take care to make sure that this method
     * can be called multiple times without hurdles.
     */
    void buildConstraint();
}
