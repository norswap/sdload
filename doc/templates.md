# Templates

Templates are JSON files that look a lot like the output generate by sdnwl, but
are allowed to use wildcards instead of actual values.

Each top-level policy in the template file represents a "template policy" that
can be selected randomly when a policy needs to be generated.

In each top-level template policy, the "likeliness" property is interpreted
specially: the probability that the policy will be used when a policy is
generated is (policy likeliness value) / (sum of all likeliness value). The
likeliness of a policy defaults to 10.