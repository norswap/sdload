package be_uclouvain_inl_sdload.examples;

import be_uclouvain_inl_sdload.ApplicationModel;
import be_uclouvain_inl_sdload.OpenFlowModel;
import be_uclouvain_inl_sdload.SDLoad;
import be_uclouvain_inl_sdload.utils.DependencyGenerator;
import be_uclouvain_inl_sdload.Settings;
import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.dlt.DLTList;
import be_uclouvain_inl_sdload.utils.ParameterHolder;
import com.beust.jcommander.Parameter;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import static be_uclouvain_inl_sdload.DSL.*;

/**
 * Model to generate input to the ESPRES scheduler. Each sub-update installs a set of rules
 * on the switches, using a particular Directed Acyclic Graph (DAG) shape to define the shape
 * of the dependencies between the different rules.
 *
 * See {@link be_uclouvain_inl_sdload.examples.EspresModel} for more info on ESPRES.
 */
public class EspresDepsModel implements ApplicationModel
{
    public Constraint constraint;

    // ---------------------------------------------------------------------------------------------
    // options

    public static class Options implements ParameterHolder
    {
        /**
         * Number of ESPRES updates to generate.
         */
        @Parameter(names = "-nUpdates")
        Supplier<Integer> nUpdates = () -> 1;

        /**
         * Minimum number of ESPRES sub-updates in each update.
         */
        @Parameter(names = "-minSubUpdates")
        public int minSubUpdates = 1000;

        /**
         * Maximum number of ESPRES sub-updates in each update.
         */
        @Parameter(names = "-maxSubUpdates")
        public int maxSubUpdates = 1000;

        /**
         * If using one of the pre-defined dependency shapes, its assigned name.
         */
        @Parameter(names = "-dependencyShapeName")
        public String dependencyShapeName = null;

        /**
         * Yields the number of rules to put inside the next sub-update.
         */
        public Supplier<Integer> subUpdateSize;

        /**
         * Used to generate the DAG that will define the inter-rule dependencies inside a
         * sub-update. The size of the generated DAG needs to be consistent with the size last
         * returned by {@link #subUpdateSize}.
         */
        public DependencyGenerator depsGen;

        @Override
        public void configure()
        {
            if (subUpdateSize == null || depsGen == null)
            {
                assert subUpdateSize == depsGen;

                shapeDictionary.getOrDefault(dependencyShapeName,
                    this::useSixDeepTreeWithDepsTowardsLeafs).run();
            }
        }

        /**
         * Associate pre-defined dependency shape names to a function that configures this
         * object to setup the associated shape (by setting the {@link #subUpdateSize} and
         * {@link #depsGen} fields). You can add your own dependency shapes in this map to be able
         * to specify them on the command line.
         */
        public final Map<String, Runnable> shapeDictionary = new HashMap<>();
        {
            shapeDictionary.put(
                "threeDeepWithDepsTowardsRoot",  this::useThreeDeepTreeWithDepsTowardsRoot);
            shapeDictionary.put(
                "threeDeepWithDepsTowardsLeafs", this::useThreeDeepTreeWithDepsTowardsLeafs);
            shapeDictionary.put(
                "sixDeepWithDepsTowardsRoot",    this::useSixDeepTreeWithDepsTowardsRoot);
            shapeDictionary.put(
                "sixDeepWithDepsTowardsLeafs",   this::useSixDeepTreeWithDepsTowardsLeafs);
            shapeDictionary.put(
                "threeStepsChain",               this::useThreeStepsChain);
            shapeDictionary.put(
                "sevenStepsChain",               this::useSevenStepsChain);
        }

        void useThreeDeepTreeWithDepsTowardsRoot()
        {
            depsGen = new DependencyGenerator();

            subUpdateSize   = () -> 7;
            depsGen.nLevels = () -> 3;

            depsGen.levelToSizeWeight =
                level -> Math.pow(2, level);

            depsGen.interLevelWeight =
                (levelSrc, levelDst) -> (double) (levelDst == levelSrc - 1 ? 1 : 0);

            depsGen.levelToNumDeps =
                level -> 1;
        }

        void useThreeDeepTreeWithDepsTowardsLeafs()
        {
            depsGen = new DependencyGenerator();

            subUpdateSize   = () -> 7;
            depsGen.nLevels = () -> 3;

            depsGen.levelToSizeWeight =
                level -> Math.pow(2, depsGen.nLevels.get() - 1 - level);

            depsGen.interLevelWeight =
                (levelSrc, levelDst) -> (double) (levelDst == levelSrc - 1 ? 1 : 0);

            depsGen.levelToNumDeps =
                level -> 2;
        }

        void useSixDeepTreeWithDepsTowardsRoot()
        {
            depsGen = new DependencyGenerator();

            subUpdateSize   = () -> 63;
            depsGen.nLevels = () -> 6;

            depsGen.levelToSizeWeight =
                level -> Math.pow(2, level);

            depsGen.interLevelWeight =
                (levelSrc, levelDst) -> (double) (levelDst == levelSrc - 1 ? 1 : 0);

            depsGen.levelToNumDeps =
                level -> 1;
        }

        void useSixDeepTreeWithDepsTowardsLeafs()
        {
            depsGen = new DependencyGenerator();

            subUpdateSize   = () -> 63;
            depsGen.nLevels = () -> 6;

            depsGen.levelToSizeWeight =
                level -> Math.pow(2, depsGen.nLevels.get() - 1 - level);

            depsGen.interLevelWeight =
                (levelSrc, levelDst) -> (double) (levelDst == levelSrc - 1 ? 1 : 0);

            depsGen.levelToNumDeps =
                level -> 2;
        }

        void useThreeStepsChain()
        {
            subUpdateSize   = () -> 3;
            depsGen.nLevels = () -> 3;

            depsGen.levelToSizeWeight =
                level -> 1.0;

            depsGen.interLevelWeight =
                (levelSrc, levelDst) -> (double) (levelDst == levelSrc - 1 ? 1 : 0);

            depsGen.levelToNumDeps =
                level -> 1;
        }

        void useSevenStepsChain()
        {
            depsGen = new DependencyGenerator();

            subUpdateSize   = () -> 7;
            depsGen.nLevels = () -> 7;

            depsGen.levelToSizeWeight =
                level -> 1.0;

            depsGen.interLevelWeight =
                (levelSrc, levelDst) -> (double) (levelDst == levelSrc - 1 ? 1 : 0);

            depsGen.levelToNumDeps =
                level -> 1;
        }
    }

    public static final Options OPTIONS = new Options();

    // ---------------------------------------------------------------------------------------------
    // model

    @Override
    public Constraint constraint()
    {
        return constraint;
    }

    @Override
    public ParameterHolder options()
    {
        return OPTIONS;
    }

    // ---------------------------------------------------------------------------------------------
    // constraint definition

    @Override
    public void buildConstraint()
    {
        OpenFlowModel openflowModel = new OpenFlowModel();
        openflowModel.options().configure();
        openflowModel.buildConstraint();
        openflowModel.command.setWeightForConstraint("type=del_rule",     0);
        openflowModel.command.setWeightForConstraint("type=query_switch", 0);
        openflowModel.command.updateGenerator();

        Constraint dependencies =
        list(list(integer()).size(2))
        .generator(parent -> OPTIONS.depsGen.generateIndexDeps((DLTList) parent.getAtPath("items")))
        .get();

        Constraint items =
        list(openflowModel.message)
        .listGenerator(OPTIONS.subUpdateSize)
        .get();

        this.constraint =
        list(
            list(
                dict()
                .key("items", items)
                .key("dependencies", dependencies)
            )
            .listGenerator(OPTIONS.minSubUpdates, OPTIONS.maxSubUpdates)
        )
        .listGenerator(OPTIONS.nUpdates)
        .get();
    }

    // ---------------------------------------------------------------------------------------------
    // main

    public static void main(String[] args)
    {
        Settings.get().fillWithCmdLineParameters(args);
        OPTIONS.fillWithCmdLineParameters(args);
        SDLoad.run(new EspresDepsModel());
    }
}
