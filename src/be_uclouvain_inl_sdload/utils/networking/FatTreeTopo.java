package be_uclouvain_inl_sdload.utils.networking;

import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;

import java.util.stream.IntStream;

/**
 *
 */
public class FatTreeTopo {

    public static class Switch extends Topology.Switch
    {
        public Switch(int pod, int sw)
        {
            super((pod<<16) + (sw<<8));
        }
    }

    private static int link_id=0;

    private static int next_link_id()
    {
        return ++link_id;
    }

    public static UndirectedGraph<Topology.Switch, Topology.Link> generate(int k)
    {
        link_id = 0;
        UndirectedGraph<Topology.Switch, Topology.Link> graph = new UndirectedSparseGraph<>();
        IntStream core_sws = IntStream.range(1, k / 2 + 1);
        IntStream agg_sws = IntStream.range(k / 2, k);
        IntStream edge_sws = IntStream.range(0, k / 2);
        for (int p = 0; p < k; p++) {
            final int pf = p;
            edge_sws.forEach(e ->
            {
                Switch edge = new Switch(pf, e);
                graph.addVertex(edge);
                agg_sws.forEach(a ->
                {
                    int link_id = next_link_id();
                    Switch agg = new Switch(pf, a);
                    graph.addVertex(agg); // works only if addVertex is idempotent
                    Topology.Link link = new Topology.Link(link_id);
                    graph.addEdge(link, edge, agg);
                });
            });

            agg_sws.forEach(a ->
            {
                Switch agg = new Switch(pf, a);
                int c_index = a - k / 2 + 1;
                core_sws.forEach(c ->
                {
                    int link_id = next_link_id();
                    Switch core = new Switch(k + c, c_index);
                    graph.addVertex(core);
                    Topology.Link link = new Topology.Link(link_id);
                    graph.addEdge(link, core, agg);
                });
            });
        }

        return graph;
    }
}
