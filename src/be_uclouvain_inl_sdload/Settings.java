package be_uclouvain_inl_sdload;

import be_uclouvain_inl_sdload.generators.UniqueSeqIDGenerator;
import be_uclouvain_inl_sdload.utils.ParameterHolder;
import be_uclouvain_inl_sdload.utils.networking.Topology;
import com.beust.jcommander.Parameter;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.io.GraphMLReader;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import org.apache.commons.collections15.Factory;

import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Hold settings that are relevant to all clients of SDLoad. This include things like defining
 * the topology & the output files.
 *
 * Application-specific settings go into "options" (see
 * {@link be_uclouvain_inl_sdload.ApplicationModel#options()}.
 */
public class Settings implements ParameterHolder
{
    // ---------------------------------------------------------------------------------------------
    // singleton

    private static Settings instance = new Settings();

    public static Settings get()
    {
        return instance;
    }

    // ---------------------------------------------------------------------------------------------
    // command-line parameters

    /**
     * The number of switches in the topology. If a topology file is supplied,
     * this will automatically be overridden by the number of switches specified by the topology.
     */
    @Parameter(names = "-nSwitches")
    public int nSwitches = 180;

    /**
     * The number of ports per switch in the topology, if a topology file is not supplied.
     */
    @Parameter(names = "-nPortsPerSwitch")
    public int nPortsPerSwitch = 24;

    /**
     * The output file where we will write the generated input as JSON. Or "-" to output to the
     * console. Defaults to the console.
     */
    @Parameter(names="-out")
    public String out = "-";

    /**
     * The name of a .graphml file containing a description of the topology to use.
     */
    @Parameter(names="-topo")
    public String topoFile = null;

    /**
     * If the output file is different from the console, duplicate the output to the console.
     */
    @Parameter(names="-console", arity = 1)
    public boolean console = false;

    /**
     * If true, do not output to a file. One can still output to the console by setting the
     * {@link #console} parameter to true.
     */
    @Parameter(names="-no-out", arity = 1)
    public boolean noOutput = false;

    /**
     * If true, pretty print the JSON output. Otherwise, make it as compact as possible (single
     * line, no spaces).
     */
    @Parameter(names="-prettyPrint", arity = 1)
    public boolean prettyPrint = true;

    // ---------------------------------------------------------------------------------------------

    private PrintStream outputStream;

    public PrintStream getOutputStream()
    {
        return outputStream;
    }

    private Topology topology;

    /**
     * The topology used for generation, or null if no topology is used and we rely on a switch
     * count and a number of ports per switch instead.
     */
    public Topology topology()
    {
        return topology;
    }

    /**
     * Sets the topology to use for generation, and updates the switch count accordingly.
     */
    public void setTopology(Topology topology)
    {
        this.topology = topology;
        this.nSwitches = topology.graph().getVertexCount();
        this.nPortsPerSwitch = -1; // cause trouble if trying to use this field with a topology
    }

    /**
     * Read a topology from a .graphml file, and sets it as the topology to use for generation.
     */
    private void readTopology(Path path)
    {
        UniqueSeqIDGenerator vGen = new UniqueSeqIDGenerator();
        UniqueSeqIDGenerator eGen = new UniqueSeqIDGenerator();
        Factory<Topology.Switch> vertexFactory   = () -> new Topology.Switch(vGen.get());
        Factory<Topology.Link>   edgeFactory     = () -> new Topology.Link(eGen.get());

        try {
            UndirectedGraph<Topology.Switch, Topology.Link> graph = new UndirectedSparseGraph<>();
            GraphMLReader<UndirectedGraph<Topology.Switch, Topology.Link>, Topology.Switch, Topology.Link> reader =
                    new GraphMLReader<>(vertexFactory, edgeFactory);
            reader.load(path.toString(), graph);
            setTopology(new Topology(graph));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }

        nSwitches = this.topology.graph().getVertexCount();
    }

    @Override
    public void configure()
    {
        if (topoFile != null) {
            readTopology(Paths.get(topoFile));
        }

        if (!noOutput)
        if (out.equals("-")) {
            outputStream = System.out;
        }
        else {
            try {
                outputStream = new PrintStream(out);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Print an object to the output stream and/or to the console, or don't do anything, depending
     * on the configuration.
     */
    void println(Object object)
    {
        if (!noOutput) {
            outputStream.println(object);
        }

        if (console && outputStream != System.out)
        {
            System.out.println(object);
        }
    }

    /**
     * Closes the output stream, if there is one, unless it is {@link System#out}.
     */
    public void closeStream()
    {
        if (outputStream != null && outputStream != System.out) {
            outputStream.close();
        }
    }
}