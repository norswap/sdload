package be_uclouvain_inl_sdload.constraints;

import be_uclouvain_inl_sdload.CheckDiagnostic;
import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTValue;
import be_uclouvain_inl_sdload.utils.PathEnabled;
import com.google.common.math.DoubleMath;

/**
 * Verifies that the DLT is an integral number (but its type is allowed to be Float or Double --
 * as long as there nothing behind the decimal point) comprised between two integer bounds. The
 * maximum bounds are the Long type bounds, which are also the default bounds.
 */
public class IntegerRangeConstraint extends AbstractConstraint
{
    public long min;
    public long max;
    public boolean checkBounds;

    public IntegerRangeConstraint()
    {
        this.checkBounds = false;
        setGenerator(parent -> new DLTValue(Randomizer.random().nextLong()));
    }

    public IntegerRangeConstraint(long min, long max)
    {
        this.checkBounds = true;
        this.min = min;
        this.max = max;
        setGenerator(parent -> new DLTValue(Randomizer.betweenClosed(min, max)));
    }

    @Override
    public CheckDiagnostic check(DLT dlt)
    {
        if (!(dlt instanceof DLTValue)) {
            return CheckDiagnostic.failed("not a DLT value");
        }

        DLTValue dltValue = (DLTValue) dlt;

        if (!(dltValue.value instanceof Number)) {
            return CheckDiagnostic.failed("not a number");
        }

        long   integer   = ((Number) dltValue.value).longValue();
        double floating  = ((Number) dltValue.value).doubleValue();

        if (!DoubleMath.isMathematicalInteger(floating)) {
            return CheckDiagnostic.failed("not an integer");
        }

        if (checkBounds && !(integer >= min && integer <= max)) {
            return CheckDiagnostic.failed("integer out of range");
        }

        return CheckDiagnostic.SATISFIED;
    }

    @Override
    public String toString()
    {
        return String.format(" [%d - %d]", min, max);
    }

    @Override
    public PathEnabled readNode(String key)
    {
        return null;
    }
}
