package be_uclouvain_inl_sdload.utils.networking;

import be_uclouvain_inl_sdload.Settings;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTDict;
import be_uclouvain_inl_sdload.dlt.DLTList;
import eu_norswap_utils.Caster;

import java.util.List;

import be_uclouvain_inl_sdload.utils.networking.Topology.Switch;

public class PathInstallation
{
    /**
     * Generate a list of OpenFlow-like messages (like in {@code DefaultModel.top.read
     * ("item/item")}) from a path and a set of matching criteria. The messages will install
     * forwarding rules to forward packets satisfying the matching criteria along the given path.
     */
    public static DLTList fromPath(List<Switch> path, DLT matchingCriteria)
    {
        DLTList installation = new DLTList(path.size() - 1);
        Topology topology = Settings.get().topology();

        Switch v1 = null;

        for (Switch v2 : path)
        {
            if (v1 == null) {
                v1 = v2;
                continue;
            }

            DLTDict message = new DLTDict();
            installation.add(message);

            message.putObject("destination", Switches.IPV4_MAKER.make(v1.id));

            DLTDict command = new DLTDict();
            message.put("command", command);
            command.putObject("type", "add_rule");

            command.put("match", Caster.cast(matchingCriteria.clone()));

            DLTDict action = new DLTDict();
            command.put("action", action);
            action.putObject("type", "forward");

            long port = (long) topology.portTowards(v1, v2);
            if (port == - 1) {
                throw new RuntimeException(String.format("Trying to install a path inconsistent " +
                        "with the underlying topology: switches %s and %s are not neighbour.",
                        v1, v2));
            }

            action.putObject("port", port);

            v1 = v2;
        }

        return installation;
    }
}
