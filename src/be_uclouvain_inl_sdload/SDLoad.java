package be_uclouvain_inl_sdload;

import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTs;
import com.cedarsoftware.util.io.JsonWriter;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * This class holds static method to be called to drive the generation process.
 */
public class SDLoad
{
    /**
     * Runs the generator associated to the model, checks it against the associated constraint,
     * then outputs the result according to the {@link Settings} singleton configuration.
     *
     * If the generated input does not match the constraint, it will be written regardless (so
     * that it can be inspected), but an error message will be written to the command line to
     * indicate the nature of the error.
     *
     * Returns the generated hierarchy.
     *
     * Be sure to close the output stream with
     * {@link be_uclouvain_inl_sdload.Settings#closeStream()}
     * unless you want to reuse it.
     */
    public static DLT run(ApplicationModel model)
    {
        // configure the global settings and application-specific options
        Settings.get().configure();
        model.options().configure();

        // build the constraint
        model.buildConstraint();

        // generate the output
        DLT output = model.constraint().generate(null);

        // check that the output is conform to the constraint
        CheckDiagnostic diag = model.constraint().check(output);
        if (!diag.satisfied()) {
            System.out.println("Error on path: " + diag);
        }

        // convert the output to json
        String json = JSONValue.toJSONString(DLTs.toVanillaHierarchy(output));

        if (Settings.get().prettyPrint)
        {
            try {
                json = JsonWriter.formatJson(json);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        // print the json to the output stream (if any is configured)
        Settings.get().println(json);

        return output;
    }

    // TODO
    public static DLT generateFromTemplate(Constraint constraint)
    {
        Path path = Paths.get("./test/test.json");
        DLT dlt;

        try (Reader reader = new BufferedReader(new FileReader(path.toFile())))
        {
            Object hierarchy = JSONValue.parse(reader);
            dlt = DLTs.from(hierarchy);

            CheckDiagnostic diag = constraint.check(dlt);

            if (!diag.satisfied()) {
                System.out.println("Template error on path: " + diag);
            }

            dlt = TemplateGenerator.get.generate(constraint, dlt);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

        return dlt;
    }
}
