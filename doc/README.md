# SND Workload Generator Documentation

## TODO

- write documentation
    - describe object model
        - a class + a registry + types singletons + specialized classes for certain types
    - explain that since we don't work with the object model, interfaces are empty
    - explain parameters, properties passthrough, programmatic settings
    + all kinds of programmatic extensions & patterns

- allow registry childs? (proxies that forward their messages to a parent by default by can
  override them -- just like inheritance) ; same for types?

 - createList & co for types? related to registry childs !