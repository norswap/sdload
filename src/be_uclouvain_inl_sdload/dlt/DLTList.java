package be_uclouvain_inl_sdload.dlt;

import eu_norswap_utils.Caster;

import java.util.ArrayList;

/**
 * A list implementing the DLT interface.
 */
public class DLTList extends ArrayList<DLT> implements DLT
{
    private DLT parent;
    private DLTPath.Component parentComponent;

    /**
     * Construct a new DLTList with some initial size.
     */
    public DLTList(int size)
    {
        super(size);
    }

    /**
     * Construct a new DLTList with the default initial size.
     */
    public DLTList() {}

    @Override
    public DLT parent()
    {
        return parent;
    }

    @Override
    public DLTPath.Component parentComponent() { return parentComponent; }

    @Override
    public void setParent(DLT parent, DLTPath.Component parentComponent)
    {
        this.parent = parent;
        this.parentComponent = parentComponent;
    }

    @Override
    public Object getAtPath(DLTPath path)
    {
        if (path.isEmpty())
        {
            return this;
        }

        if (path.head() instanceof DLTPath.Parent)
        {
            return parent.getAtPath(path.tail());
        }

        if (path.head() instanceof DLTPath.Index)
        {
            int index = Caster.<DLTPath.Index>cast(path.head()).index;

            if (index >= size()) {
                throw new RuntimeException("DLT list index out of bounds: " + index
                        + " (list: " + this + ")");
            }

            DLT sub = get(index);

            return sub.getAtPath(path.tail());
        }

        throw new RuntimeException("Wrong path component for DLT list: "
                + path.head().getClass()  + " (list: " + this + ")");
    }

    @Override
    public boolean add(DLT dlt)
    {
        dlt.setParent(this, new DLTPath.Index(size()));
        return super.add(dlt);
    }

    public boolean addObject(Object object)
    {
        DLT dlt = DLTs.from(object);
        return add(dlt);
    }

    @Override
    public DLTList clone()
    {
        DLTList out = Caster.cast(super.clone());
        out.parent = parent;
        return out;
    }
}
