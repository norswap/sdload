package eu_norswap_utils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * Misc. utilities to work with strings.
 */
public class Numbers
{
    public static List<Integer> range(int startInclusive, int endExclusive)
    {
        return streamToList(IntStream.range(startInclusive, endExclusive));
    }

    public static List<Integer> rangeClosed(int startInclusive, int endInclusive)
    {
        return streamToList(IntStream.rangeClosed(startInclusive, endInclusive));
    }

    public static List<Integer> streamToList(IntStream stream)
    {
        return stream.boxed().collect(Collectors.toList());
    }

    public static List<Long> streamToList(LongStream stream)
    {
        return stream.boxed().collect(Collectors.toList());
    }

    public static List<Double> streamToList(DoubleStream stream)
    {
        return stream.boxed().collect(Collectors.toList());
    }
}
