package be_uclouvain_inl_sdload.dlt;

import be_uclouvain_inl_sdload.utils.ConsList;

/**
 * A DLT path represents a path from a node to another in a DLT tree. A path is always relative
 * to a node in the tree. Absolute paths can be encoded as paths relative to the root.
 *
 * A path is in fact a list of components. Each component can either be a key, an index, or
 * a reference to the parent. Empty DLT paths are valid, and simply reference the node they are
 * relative to.
 *
 * DLT paths can be represented as strings, using "/" as the components separator and ".." as
 * parent reference. Index are denoted by numbers, and all other chains of characters between two
 * separators (or a separator and the begin/end of the string) represent keys.
 *
 * Design discussion: it would not make sense to encode absolute paths explicitly, given that DLT
 * nodes do not hold direct references to the root.
 */
public class DLTPath extends ConsList<DLTPath.Component, DLTPath>
{
    public static class MetaDLTPath extends MetaConsList<Component, DLTPath>
    {
        @Override
        public DLTPath cons(Component head, DLTPath tail)
        {
            return new DLTPath(head, tail);
        }

        @Override
        public DLTPath empty()
        {
            return new DLTPath();
        }
    }

    private static MetaDLTPath META = new MetaDLTPath();

    public static <T> MetaDLTPath META()
    {
        return META;
    }

    @Override
    public MetaDLTPath meta()
    {
        return META();
    }

    public static interface Component {}

    public static class Key implements Component
    {
        public String key;

        public Key(String key)
        {
            this.key = key;
        }

        @Override
        public String toString()
        {
            return key;
        }
    }

    public static class Index implements Component
    {
        public int index;

        public Index(int index)
        {
            this.index = index;
        }

        @Override
        public String toString()
        {
            return String.valueOf(index);
        }
    }

    public static class Parent implements Component
    {
        public static final Parent get = new Parent();

        @Override
        public String toString()
        {
            return "..";
        }
    }

    public static final DLTPath EMPTY = new DLTPath();

    protected DLTPath() {}

    public DLTPath(Component head, DLTPath tail)
    {
        super(head, tail);
    }

    /**
     * Create a DLT path from a string representation of this path.
     */
    public static DLTPath from(String path)
    {
        DLTPath out = EMPTY;
        String[] strComponents = path.split("/");

        for (String s : strComponents)
        {
            if (s.equals(".."))  {
                out = out.prepend(Parent.get);
                continue;
            }

            try  {
                int index = Integer.parseInt(s);
                out = out.prepend(new Index(index));
                continue;
            }
            catch (NumberFormatException e) {}

            out = out.prepend(new Key(s));
        }

        return out;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        for (Component c : this) {
            builder.append("/");
            builder.append(c);
        }

        if (this.isEmpty()) {
            builder.append("/");
        }

        return builder.toString();
    }
}
