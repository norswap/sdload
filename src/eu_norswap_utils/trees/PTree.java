package eu_norswap_utils.trees;

/**
 *
 */
public interface PTree<Self extends PTree<Self>> extends Tree<Self>
{
    Self parent();

    default boolean isRoot()
    {
        return parent() == null;
    }

    static class Implem<Self extends Implem<Self>> extends Tree.Implem<Self> implements PTree<Self>
    {
        protected Self parent;

        @Override
        public Self parent()
        {
            return parent;
        }
    }
}
