package be_uclouvain_inl_sdload.utils;

import eu_norswap_utils.Caster;

/**
 * An immutable linked-list implementation using the cons principle: a list is either the empty
 * list or a (value,list) pair.
 */
class Cons<T> extends ConsList<T, Cons<T>>
{
    public static class MetaCons<T> extends MetaConsList<T, Cons<T>>
    {
        @Override
        public Cons<T> cons(T head, Cons<T> tail)
        {
            return new Cons<>(head, tail);
        }

        @Override
        public Cons<T> empty()
        {
            return new Cons<>();
        }
    }

    private static MetaCons<Object> META = new MetaCons<>();

    public static <T> MetaCons<T> META()
    {
        return Caster.cast(META);
    }

    @Override
    public MetaCons<T> meta()
    {
        return META();
    }

    protected Cons() {}

    public Cons(T head, Cons<T> tail)
    {
        super(head, tail);
    }
}
