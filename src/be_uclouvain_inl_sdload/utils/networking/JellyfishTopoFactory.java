package be_uclouvain_inl_sdload.utils.networking;

import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;

import static be_uclouvain_inl_sdload.utils.networking.Topology.Link;
import static be_uclouvain_inl_sdload.utils.networking.Topology.Switch;

public class JellyfishTopoFactory {

    private static int link_id=0;

    private static int next_link_id()
    {
        return ++link_id;
    }

    /**
     * Returns a uniformly random <tt>k</tt>-regular graph on <tt>V</tt> vertices
     * (not necessarily simple). The graph is simple with probability only about e^(-k^2/4),
     * which is tiny when k = 14.
     * For additional documentation, see <a href="http://algs4.cs.princeton.edu/41undirected">Section 4.1</a> of
     * <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
     * @param V the number of vertices in the graph
     * @return a uniformly random <tt>k</tt>-regular graph on <tt>V</tt> vertices.
     */
    public static UndirectedGraph<Topology.Switch, Topology.Link> generate(int V, int k) {
        if (V*k % 2 != 0) V = V+1;
        //throw new IllegalArgumentException("Number of vertices * k must be even");
        UndirectedGraph<Switch, Link> G = new UndirectedSparseGraph<>();

        // create k copies of each vertex
        Topology.Switch[] vertices = new Topology.Switch[V*k];
        for (int v = 0; v < V; v++) {
            Switch sw = new Switch(v);
            G.addVertex(sw);
            for (int j = 0; j < k; j++) {
                vertices[v + V*j] = sw;
            }
        }

        // pick a random perfect matching
        StdRandom.shuffle(vertices);
        for (int i = 0; i < V*k/2; i++) {
            Topology.Link link = new Topology.Link(next_link_id());
            G.addEdge(link, vertices[2*i], vertices[2*i + 1]);
        }
        return G;
    }
}
