package be_uclouvain_inl_sdload.generators.networking;

import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.Settings;
import be_uclouvain_inl_sdload.utils.networking.Topology;

import java.util.List;
import java.util.function.Supplier;

/**
 * A generator of list of OpenFlow-like messages (as described in the default model) that install
 * forwarding rules between two nodes for a flow along (one of) the shortest path between those
 * nodes.
 *
 * For this, a topology needs to be defined in the settings.
 */
public class ShortestPathGenerator implements Supplier<List<Topology.Switch>>
{
    public static final ShortestPathGenerator get = new ShortestPathGenerator();

    @Override
    public List<Topology.Switch> get()
    {
        Topology topology = Settings.get().topology();
        assert topology.graph().getVertexCount() > 1;

        Topology.Switch src = Randomizer.randomElem(topology.graph().getVertices());
        Topology.Switch dst = src;

        while (src == dst) {
            dst = Randomizer.randomElem(topology.graph().getVertices());
        }

        return topology.shortestPath(src, dst);
    }
}
