package be_uclouvain_inl_sdload;

import be_uclouvain_inl_sdload.dlt.DLTPath;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class CheckDiagnostic
{
    public String description;

    private boolean satisfied = false;

    private DLTPath path;

    private List<CheckDiagnostic> subDiagnostics;

    public static final CheckDiagnostic SATISFIED = new CheckDiagnostic(true, "");

    public static CheckDiagnostic failed(String description)
    {
        return new CheckDiagnostic(false, description);
    }

    private CheckDiagnostic(boolean satisfied, String description)
    {
        this.satisfied = satisfied;
        this.path = DLTPath.EMPTY;
        this.description = description;
    }

    public CheckDiagnostic(String key, String description)
    {
        this.path = new DLTPath(new DLTPath.Key(key), DLTPath.EMPTY);
        this.description = description;
    }

    public CheckDiagnostic(int index, String description)
    {
        this.path = new DLTPath(new DLTPath.Index(index), DLTPath.EMPTY);
        this.description = description;
    }

    public CheckDiagnostic(String key, CheckDiagnostic child, String description)
    {
        DLTPath.Key keyPath = new DLTPath.Key(key);
        this.path = new DLTPath(keyPath, child.path);
        this.description = description;
        initSubDiagnosticsFromChild(child, keyPath);
    }

    public CheckDiagnostic(int index, CheckDiagnostic child, String description)
    {
        DLTPath.Index indexPath = new DLTPath.Index(index);
        this.path = new DLTPath(indexPath, child.path);
        this.description = description;
        initSubDiagnosticsFromChild(child, indexPath);
    }

    private void initSubDiagnosticsFromChild(CheckDiagnostic child, DLTPath.Component component)
    {
        this.subDiagnostics = child.subDiagnostics();

        if (subDiagnostics() == null) {
            this.subDiagnostics = new ArrayList<>(1);
            subDiagnostics.add(child);
        }

        subDiagnostics.stream().forEach(diag -> diag.path = new DLTPath(component, diag.path));
    }

    public CheckDiagnostic(List<CheckDiagnostic> subs, String description)
    {
        this.path = DLTPath.EMPTY;
        this.description = description;
        this.subDiagnostics = new ArrayList<>(subs.size());

        subs.stream().forEach(diag ->
        {
            if (diag.subDiagnostics() != null) {
                this.subDiagnostics.addAll(diag.subDiagnostics());
            }
            else {
                this.subDiagnostics.add(diag);
            }
        });
    }

    public boolean satisfied()
    {
        return satisfied;
    }

    public DLTPath path()
    {
        return path;
    }

    public List<CheckDiagnostic> subDiagnostics()
    {
        return subDiagnostics;
    }

    @Override
    public String toString()
    {
        return path.toString() + " (" + description + ")"
                +  (subDiagnostics == null ? "" : " " + subDiagnostics());
    }
}
