package be_uclouvain_inl_sdload.utils;

import eu_norswap_utils.Caster;

/**
 * An abstract superclass for things with a integer identifier.
 */
public abstract class Identifiable<T extends Identifiable<T>> implements Comparable<T>
{
    public final int id;

    public Identifiable(int id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object o)
    {
        return o instanceof Identifiable<?> && Caster.<Identifiable<?>>cast(o).id == id;
    }

    @Override
    public int hashCode()
    {
        return new Integer(id).hashCode();
    }

    @Override
    public int compareTo(T o)
    {
        return id - o.id;
    }

    @Override
    public String toString()
    {
        return String.valueOf(id);
    }
}
