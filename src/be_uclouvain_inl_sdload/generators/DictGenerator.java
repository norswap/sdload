package be_uclouvain_inl_sdload.generators;

import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.constraints.DictConstraint;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTDict;

/**
 * The default dictionary generator used by
 * {@link be_uclouvain_inl_sdload.constraints.DictConstraint}.
 *
 * Builds a dictionary where all the mandatory keys are present, and optional keys are present
 * with a probability equal to the value of their weight property (defaults to 0).
 */
public class DictGenerator implements DLTGenerator
{
    private DictConstraint constraint;

    public DictGenerator(DictConstraint constraint)
    {
        this.constraint = constraint;
    }

    @Override
    public DLT generate(DLT parent)
    {
        // Build a new dictionary filled with the mandatory keys.

        DLTDict out = constraint.keyConstraints.entrySet().stream().collect(
            DLTDict::new,
            (dict, entry) -> dict.put(entry.getKey(), entry.getValue().generate(dict)),
            // DLTDict::putAll); // better, but must workaround an IDEA bug
            (d1, d2) -> d1.putAll(d2));

        // Add optional keys depending on their weight, using a default of 0.

        constraint.optKeyConstraints.entrySet().stream().collect(
            () -> out,
            (dict, entry) ->
            {
                double weight = (double) entry.getValue().properties().getOrDefault("weight", 0.0);

                if (Randomizer.random().nextDouble() > weight) {
                    dict.put(entry.getKey(), entry.getValue().generate(dict));
                }
            },
            (d1, d2) -> d1.putAll(d2));

        return out;
    }
}
