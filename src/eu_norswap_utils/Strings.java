package eu_norswap_utils;

/**
 * Misc. utilities to work with strings.
 */
public class Strings
{
    /**
     * Tries to return the integer value of the string, or null if the string does not represent
     * and integer.
     */
    public static Integer parseInt(String s)
    {
        try {
            return Integer.parseInt(s);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Tries to return the integer value of the string, or throws a runtime exception if
     * the string does not represent an integer.
     */
    public static Integer parseInt_dyn(String s)
    {
        try {
            return Integer.parseInt(s);
        }
        catch (NumberFormatException e) {
            throw new RuntimeException(e);
        }
    }
}
