package eu_norswap_utils.trees;

import eu_norswap_utils.Caster;
import eu_norswap_utils.Reverse;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.RandomAccess;
import java.util.Stack;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Models a tree as a node holding a list of sub-nodes, and provides methods to visit the tree.
 */
public interface Tree<Self extends Tree<Self>>
{
    /** Must not be null. */
    <T extends List<Self> & RandomAccess> T children();

    default boolean isLeaf()
    {
        List<Self> children = children();
        return children.isEmpty();
    }

    /**
     * Implements the {@link Tree} interface by providing storage for the children list.
     * The children list must no
     */
    static class Implem<Self extends Tree<Self>> implements Tree<Self>
    {
        protected ArrayList<Self> children;

        @Override
        public <T extends List<Self> & RandomAccess> T children()
        {
            return Caster.cast(children);
        }
    }

    /**
     * Calls {@code consumer} on all nodes of the tree, following a prefix depth-first order:
     * the consumer is called on a node before being called on its children, and is called on
     * the descendants of a node before being called on the right siblings of that node.
     */
    default void prefixDepthFirstVisit(Consumer<? super Self> consumer)
    {
        Stack<Self> stack = new Stack<>();
        stack.push(Caster.cast(this));

        while (!stack.empty())
        {
            Self node = stack.pop();
            consumer.accept(node);
            // Could use method reference if IDEA wasn't so dumb.
            Reverse.forEach(node.children(), (Self x) -> stack.push(x));
        }
    }

    /**
     * Calls {@code consumer} on all nodes of the tree, following a postfix depth-first order:
     * the consumer is called on all the descendants of a node before being called on the node
     * itself, and is called on a node before being called on its right siblings or their
     * descendants.
     */
    default void postfixDepthFirstVisit(Consumer<? super Self> consumer)
    {
        prepostDepthFirstVisit(null, consumer);
    }

    /**
     * Performs a depth-first visit of the tree, calling {@code prefixConsumer} in prefix
     * order and {@code postfixConsumer} in postfix order. The prefix consumer can be null, which
     * is how the postfix-only visit is implemented.
     *
     * See {@link #prefixDepthFirstVisit(java.util.function.Consumer)} and
     * {@link #postfixDepthFirstVisit(java.util.function.Consumer)}.
     */
    default void prepostDepthFirstVisit(
        Consumer<? super Self> prefixConsumer,
        Consumer<? super Self> postfixConsumer)
    {
        Stack<Self> stack = new Stack<>();
        Stack<Integer> indices = new Stack<>();

        Self node = Caster.<Self>cast(this);
        int index = 0;

        while (node != null || !stack.isEmpty())
        {
            if (node == null) {
                node = stack.pop();
                index = indices.pop();
            }

            if (prefixConsumer != null && index == 0) {
                prefixConsumer.accept(node);
            }

            if (index < node.children().size())
            {
                stack.push(node);
                indices.push(index + 1);
                node = node.children().get(index);
                index = 0;
            }
            else {
                postfixConsumer.accept(node);
                node = null;
            }
        }
    }

    /**
     * Perform a depth-first visit of the tree, calling {@code prefixConsumer} in prefix
     * order and {@code postfixConsumer} in postfix order, but relaxing any constraint on the
     * ordering between siblings, which may now be visited in parallel.
     *
     * Either the prefix or postfix consumers can be null.
     *
     * Waits for the visit to terminate before returning.
     *
     * See {@link #prepostDepthFirstVisit(java.util.function.Consumer, java.util.function.Consumer)}.
     *
     * TODO
     *   Benchmark this against the sequential version using different tree sizes and reducer
     *   execution time.
     */
    default void prepostConcurrentDepthFirstVisit(
        Consumer<? super Self> prefixConsumer,
        Consumer<? super Self> postfixConsumer)
    {
        class PrePostTask extends RecursiveAction
        {
            Self node;

            PrePostTask(Self node)
            {
                this.node = node;
            }

            /**
             * This indirection is necessary so that we can use "this::create", which references
             * "ReduceTask.this", which itself holds an  implicit reference to
             * "Tree.this"; whereas "ReduceTask::new" does not reference any enclosing instance
             * of "Tree".
             */
            PrePostTask create(Self node)
            {
                return new PrePostTask(node);
            }

            @Override
            protected void compute()
            {
                if (prefixConsumer != null)
                    prefixConsumer.accept(node);

                // Could use method reference if IDEA wasn't so dumb.
                List<PrePostTask> tasks = children().stream()
                    .map(this::create).collect(Collectors.toList());

                ForkJoinTask.invokeAll(tasks);

                if (postfixConsumer != null)
                    postfixConsumer.accept(node);
            }
        }

        new PrePostTask(Caster.cast(this)).invoke();
    }

    /**
     * Calls {@code consumer} on all nodes of the tree, following an infix depth-first infix:
     * for each node, the consumer is first called on its first child, then on the node itself,
     * then on its other children.
     */
    default void infixDepthFirstVisit(Consumer<? super Self> consumer)
    {
        Stack<Self> stack = new Stack<>();
        Stack<Integer> indices = new Stack<>();

        Self node = Caster.<Self>cast(this);
        int index = 0;

        while (node != null || !stack.isEmpty())
        {
            if (node == null) {
                node = stack.pop();
                index = indices.pop();
            }

            if (node.isLeaf()) {
                consumer.accept(node);
                node = null;
            }
            else if (index == 0)
            {
                stack.push(node);
                indices.push(1);
                node = node.children().get(0);
                index = 0;
            } else {
                consumer.accept(node);
                // Could use method reference if IDEA wasn't so dumb.
                Reverse.forEach(node.children(), (Self x) -> stack.push(x));
                stack.pop(); // we already treated the first child
                for (int i = 1 ; i < node.children().size() ; ++i) indices.push(0);
                node = null;
            }
        }
    }

    /**
     * Calls {@code consumer} on all nodes of the tree, following a breadth-first order:
     * the consumer is called on all nodes at the same level in a left-to-right order, and
     * is called on all nodes of a level before being called on nodes at a deeper level.
     */
    default void breadthFirstVisit(Consumer<? super Self> consumer)
    {
        Queue<Self> queue = new ArrayDeque<>();
        queue.add(Caster.cast(this));

        while (!queue.isEmpty())
        {
            Self node = queue.remove();
            consumer.accept(node);
            queue.addAll(node.children());
        }
    }

    /**
     * Perform a reduction on the tree. A reduction is an operation that maps each node to a value
     * that depends on the node and on the result of the reduction of its children. Return the
     * result of the reduction of the root.
     *
     * Additionally, if {@code setup} is non-null
     */
    default <T> T reduce(Consumer<Self> setup, BiFunction<Self, List<T>, T> reducer)
    {
        Stack<List<T>> stack = new Stack<>();

        stack.push(new ArrayList<>(1));

        prepostDepthFirstVisit(
            (node) -> {
                if (setup != null)
                    setup.accept(node);
                stack.push(new ArrayList<>(node.children().size()));
            },

            (node) -> {
                List<T> childrenReductions = stack.pop();
                stack.peek().add(reducer.apply(node, childrenReductions));
            });

        return stack.peek().get(0);
    }

    /**
     * Perform a reduction on the tree that may run in parallel, using fork/join tasks
     * ({@link java.util.concurrent.ForkJoinTask}).
     *
     * See {@link #reduce(java.util.function.Consumer, java.util.function.BiFunction)}.
     *
     * Note this uses call stack space proportional to the tree depth, meaning you could run
     * into stack overflows with some degenerate trees (such as a super large linked list).
     *
     * TODO
     *   Benchmark this against the sequential version using different tree sizes and reducer
     *   execution time.
     */
    default <T> T reduceConcurrent(Consumer<Self> setup, BiFunction<Self, List<T>, T> reducer)
    {
        class ReduceTask extends RecursiveTask<T>
        {
            Self node;

            ReduceTask(Self node)
            {
                this.node = node;
            }

            /**
             * This indirection is necessary so that we can use "this::create", which references
             * "ReduceTask.this", which itself holds an  implicit reference to
             * "Tree.this"; whereas "ReduceTask::new" does not reference any enclosing instance
             * of "Tree".
             */
            ReduceTask create(Self node)
            {
                return new ReduceTask(node);
            }

            @Override
            protected T compute()
            {
                if (setup != null)
                    setup.accept(node);

                // Could use method reference if IDEA wasn't so dumb.
                List<ReduceTask> tasks = children().stream()
                    .map(this::create).collect(Collectors.toList());

                ForkJoinTask.invokeAll(tasks);
                return reducer.apply(node,
                    tasks.stream().map(RecursiveTask::getRawResult).collect(Collectors.toList()));
            }


        }

        return new ReduceTask(Caster.cast(this)).invoke();
    }
}
