package be_uclouvain_inl_sdload.examples;

import be_uclouvain_inl_sdload.ApplicationModel;
import be_uclouvain_inl_sdload.OpenFlowModel;
import be_uclouvain_inl_sdload.SDLoad;
import be_uclouvain_inl_sdload.Settings;
import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.dlt.DLTList;
import be_uclouvain_inl_sdload.generators.UniqueSeqIDGenerator;
import be_uclouvain_inl_sdload.utils.ParameterHolder;
import be_uclouvain_inl_sdload.utils.networking.PathInstallation;
import be_uclouvain_inl_sdload.generators.networking.ShortestPathGenerator;
import com.beust.jcommander.Parameter;
import eu_norswap_utils.Caster;

import java.util.function.Supplier;

import static be_uclouvain_inl_sdload.DSL.*;

/**
 * Model to generate input to the ESPRES scheduler. Each sub-update installs a shortest
 * path between two nodes in the topology, using per-packet consistent updates (the rule to be
 * installed on the ingress switch depends on all other rules).
 *
 * Reminder: The input of ESPRES is a sequence of updates. Each updates contains sub-updates.
 * Each sub-update contains a set of OpenFlow commands (since these commands usually install
 * OpenFlow rules, we sometimes abusively say that sub-updates contain rules), as well as a DAG
 * specifying the dependencies between these commands. A dependent command can only run after
 * all the commands depended upon have run.
 *
 * In the version of ESPRES that was tested (circa April 2014),
 * updates and sub-updates are never inter-dependant.
 */
public class EspresModel implements ApplicationModel
{
    public Constraint constraint;

    // ---------------------------------------------------------------------------------------------
    // options

    public static class Options implements ParameterHolder
    {
        /**
         * Number of ESPRES updates to generate.
         */
        @Parameter(names = "-nUpdates")
        public int nUpdates = 1;

        /**
         * Minimum number of ESPRES sub-updates in each update.
         */
        @Parameter(names = "-minSubUpdates")
        public int minSubUpdates = 1000;

        /**
         * Maximum number of ESPRES sub-updates in each update.
         */
        @Parameter(names = "-maxSubUpdates")
        public int maxSubUpdates = 1000;

        /**
         * A generator of VLAN IDs. A unique VLAN ID is used to identify each path.
         */
        public Supplier<Integer> vlanIDGenerator = null;

        @Override
        public void configure()
        {
            if (vlanIDGenerator == null) {
                vlanIDGenerator = new UniqueSeqIDGenerator();
            }
        }
    }

    public static final Options OPTIONS = new Options();

    // ---------------------------------------------------------------------------------------------
    // options

    @Override
    public Constraint constraint()
    {
        return constraint;
    }

    @Override
    public ParameterHolder options()
    {
        return OPTIONS;
    }

    // ---------------------------------------------------------------------------------------------
    // constraint definition

    @Override
    public void buildConstraint()
    {
        OpenFlowModel openflowModel = new OpenFlowModel();
        openflowModel.options().configure();
        openflowModel.buildConstraint();

        Constraint dependencies =
        list(list(integer()).size(2))
        .generator(parent ->
        {
            DLTList items = Caster.cast(parent.getAtPath("items"));
            DLTList out = new DLTList(items.size());

            for (int i = 1; i < items.size(); ++i)
            {
                out.add(dltList(0, i));
            }

            return out;
        })
        .get();

        Constraint items =
        list(openflowModel.message)
        .generator(compose(
            ShortestPathGenerator.get,
            shortestPath -> PathInstallation.fromPath(
                shortestPath,
                dltDict("vlan_id", OPTIONS.vlanIDGenerator.get()))))
        .get();

        this.constraint =
        list(
            list(
                dict()
                    .key("items", items)
                    .key("dependencies", dependencies)
            )
            .listGenerator(OPTIONS.minSubUpdates, OPTIONS.maxSubUpdates)
        )
        .listGenerator(OPTIONS.nUpdates, OPTIONS.nUpdates)
        .get();
    }

    // ---------------------------------------------------------------------------------------------
    // main

    public static void main(String[] args)
    {
        Settings.get().topoFile = "./test/ibm.graphml";
        OPTIONS.fillWithCmdLineParameters(args);
        Settings.get().fillWithCmdLineParameters(args);
        SDLoad.run(new EspresModel());
    }
}
