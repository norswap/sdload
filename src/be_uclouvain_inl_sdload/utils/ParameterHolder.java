package be_uclouvain_inl_sdload.utils;

import com.beust.jcommander.JCommander;

/**
 * Interface to be implemented by objects that hold parameters. Such objects typically hold
 * parameters that can be set from the command line using
 * <a href="http://jcommander.org/">JCommander</a>.
 */
public interface ParameterHolder
{
    /**
     * Configure this object. Essentially this consists of synthesizing some field of the objects
     * from other fields (although one could include other setup in the configuration). It must
     * be done after the required fields have been set. It can be called multiple times,
     * for multiple (sequential) runs of a program.
     */
    default void configure() {}

    /**
     * Fill in some of the parameters with JCommander using command line parameters. You should pass
     * the parameter of the {@code main()} method (or arguments laid out similarly) to this method.
     */
    default void fillWithCmdLineParameters(String[] cmdLineParameters)
    {
        Implem.fillWithCmdLineParameters(this, cmdLineParameters);
    }

    class Implem
    {
        public static void fillWithCmdLineParameters(
            ParameterHolder self, String[] cmdLineParameters)
        {
            new JCommander(self, cmdLineParameters);
        }
    }
}
