package be_uclouvain_inl_sdload.generators.networking;

import be_uclouvain_inl_sdload.utils.networking.Topology;
import be_uclouvain_inl_sdload.utils.networking.Weighter;

import java.util.List;
import java.util.function.Supplier;

/**
 * A generator of random network paths in the networks, obtained by re-weighting the links then
 * by obtained a random shortest path on the re-weighted graphs. Discards previous link weights,
 * and resets all weights to 1 after generating the random path.
 */
public class RandomPathGenerator implements Supplier<List<Topology.Switch>>
{
    public static final RandomPathGenerator get = new RandomPathGenerator();

    public int minWeight = 1;
    public int maxWeight = 100;

    @Override
    public List<Topology.Switch> get()
    {
        Weighter.randomlyWeightGraph(minWeight, maxWeight);
        List<Topology.Switch> out = ShortestPathGenerator.get.get();
        Weighter.uniformizeWeights();
        return out;
    }
}
