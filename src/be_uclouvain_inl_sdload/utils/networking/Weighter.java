package be_uclouvain_inl_sdload.utils.networking;

import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.Settings;

public class Weighter
{
    /**
     * Randomly changes the weights of the links in the topology (which influence the shortest
     * paths) to be in the [minWeight, maxWeight] range. This can be used to generate random
     * paths in the network.
     */
    public static void randomlyWeightGraph(int minWeight, int maxWeight)
    {
        Topology topology = Settings.get().topology();
        for (Topology.Link link : topology.graph().getEdges())
        {
            topology.weights.put(link.id, Randomizer.betweenClosed(minWeight, maxWeight));
        }
        topology.reloadLinkWeights();
    }

    /**
     * Sets all link weights to 1.
     */
    public static void uniformizeWeights()
    {
        Topology topology = Settings.get().topology();
        for (Topology.Link link : topology.graph().getEdges())
        {
            topology.weights.put(link.id, 1);
        }
        topology.reloadLinkWeights();
    }
}
