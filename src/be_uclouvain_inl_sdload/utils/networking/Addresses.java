package be_uclouvain_inl_sdload.utils.networking;

import java.util.function.Predicate;

/**
 * Utilities to decode addresses (currently only IPv4).
 */
public class Addresses
{
    /**
     * Return an array of the components of the IPv4 address represented by the string. The
     * components should be separated by a single dot. Return null if the string does not
     * represent an IPv4 address.
     */
    public static int[] parseIPv4(String ip)
    {
        int[] out = new int[4];
        String[] components = ip.split("\\.");

        if (components.length != 4) {
            return null;
        }

        try {
            for (int i = 0 ; i < 4 ; ++i)
            {
                int num = Integer.parseInt(components[i]);

                if (num > 255 || num < 0) {
                    return null;
                }

                out[i] = num;
            }
        }
        catch (NumberFormatException e) {
            return null;
        }

        return out;
    }

    /**
     * See {@link #parseIPv4(String)}.
     * Throws an exception if the string does not represent an IPv4 address.
     */
    public static int[] parseIPv4_dyn(String ip)
    {
        int[] out = parseIPv4(ip);

        if (out == null) {
            throw new IllegalArgumentException("not an IPv4: " + ip);
        }

        return out;
    }

    /**
     * A predicate that indicates if a string is a valid IPv4 address.
     */
    public static final Predicate<String> IS_IPV4 = string -> parseIPv4(string) != null;
}
