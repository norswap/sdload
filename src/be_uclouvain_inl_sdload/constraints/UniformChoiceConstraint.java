package be_uclouvain_inl_sdload.constraints;

import eu_norswap_utils.probabilities.Randomizer;

import java.util.List;

/**
 * A choice constraint associated to a generator that selects one of the alternatives with
 * uniform probability. The selected alternative's generator is then to construct the
 * resulting value.
 *
 * The generator is backed by the alternatives list of the constraint, so there is no need
 * to update the generator when the alternatives list changes or is replaced.
 */
public class UniformChoiceConstraint extends ChoiceConstraint
{
    private List<Constraint> alternatives;

    public UniformChoiceConstraint(List<Constraint> alternatives)
    {
        setAlternatives(alternatives);

        setGenerator(parent -> {
            int index = Randomizer.random().nextInt(this.alternatives.size());
            return this.alternatives.get(index).generate(parent);
        });
    }

    public void setAlternatives(List<Constraint> alternatives)
    {
        this.alternatives = alternatives;
    }

    /**
     * {@inheritDoc} ({@link UniformChoiceConstraint} -> modifiable)
     */
    @Override
    public List<Constraint> alternatives()
    {
        return alternatives;
    }
}
