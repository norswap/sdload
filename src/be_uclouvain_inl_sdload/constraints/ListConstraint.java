package be_uclouvain_inl_sdload.constraints;

import be_uclouvain_inl_sdload.CheckDiagnostic;
import be_uclouvain_inl_sdload.Holes;
import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTList;
import eu_norswap_utils.Caster;
import be_uclouvain_inl_sdload.utils.PathEnabled;

import java.util.function.Supplier;

/**
 * A constraint for list objects that allows setting a sub-constraint that all list elements
 * should satisfy, as well as size bounds.
 */
public class ListConstraint extends AbstractConstraint
{
    private boolean hasMin = false;
    private boolean hasMax = false;
    private int minSize;
    private int maxSize;

    /**
     * The constraint that each element of the list must satisfy.
     */
    public Constraint itemConstraint;

    /**
     * Builds a new list constraint with a constraint that each element of the list must satisfy.
     */
    public ListConstraint(Constraint itemConstraint)
    {
        this.itemConstraint = itemConstraint;
    }

    @Override
    public CheckDiagnostic check(DLT dlt)
    {
        if (!(dlt instanceof DLTList)) {
            return CheckDiagnostic.failed("not a DLT list");
        }

        DLTList list = Caster.cast(dlt);

        if (hasMin && minSize > list.size() || hasMax && list.size() > maxSize) {
            return CheckDiagnostic.failed("wrong size");
        }

        int i = 0;
        for (DLT item : list)
        {
            CheckDiagnostic diag = checkSubConstraint(itemConstraint, item);

            if (!diag.satisfied()) {
                return new CheckDiagnostic(i, diag, "failed item constraint");
            }
            ++i;
        }

        return CheckDiagnostic.SATISFIED;
    }

    /**
     * Check if a constraint applies on a child value -- overridable by subclasses.
     */
    protected CheckDiagnostic checkSubConstraint(Constraint constraint, DLT value)
    {
        return !(constraint instanceof ValueConstraint) && Holes.isHole(value)
                ? CheckDiagnostic.SATISFIED
                : constraint.check(value);
    }

    @Override
    public String toString()
    {
        return String.format("list[%s] [%s - %s]", itemConstraint,
                hasMin ? minSize : 0,
                hasMax ? maxSize : "∞");
    }

    // SIZE HANDLING

    /**
     * Sets this constraint's generator to be a generator that generates lists whose size
     * in the [minSize, maxSize] range; using the generator of this constraint's sub-constraint.
     *
     * ! Those sizes only constraint the generator, and do not influence whether a DLT satisfy
     * this constraint or not. To achieve this result, use {@link #setSize(int, int)} and
     * {@link #setGeneratorFromBounds()}.
     */
    public void setGenerator(int minSize, int maxSize)
    {
        setGeneratorFrom(() -> (int) Randomizer.betweenClosed(minSize, maxSize));
    }

    /**
     * Like {@link #setGenerator(int, int)}, but using the minSize and maxSize bounds of
     * the constraint instead of explicit parameters.
     */
    public void setGeneratorFromBounds()
    {
        if (!hasMin || !hasMax) {
            throw new RuntimeException("Attempting to make a list generator for a list without " +
                    "both minimum and maximum size bounds.");
        }

        setGenerator(minSize, maxSize);
    }

    /**
     * Sets this constraint's generator to be a generator that generates lists whose size
     * is determined by the given supplier; using the generator of this constraint's sub-constraint.
     */
    public void setGeneratorFrom(Supplier<Integer> sizeSupplier)
    {
        setGenerator(parent ->
        {
            int size = sizeSupplier.get();
            DLTList list = new DLTList(size);
            for (int i = 0 ; i < size ; ++i) {
                list.add(itemConstraint.generate(list));
            }
            return list;
        });
    }

    public void setSize(int x)
    {
        setSize(x, x);
    }

    public void setSize(int min, int max)
    {
        this.hasMin = this.hasMax = true;
        this.minSize = min;
        this.maxSize = max;
    }

    public void setMinSize(int x)
    {
        this.hasMin = true;
        this.minSize = x;
        this.maxSize = Integer.MAX_VALUE;
    }

    public void setMaxSize(int x)
    {
        this.hasMax = true;
        this.minSize = 0;
        this.maxSize = x;
    }

    public boolean hasMinBound()
    {
        return hasMin;
    }

    public boolean hasMaxBound()
    {
        return hasMax;
    }

    public void removeBounds()
    {
        this.hasMin = false;
        this.hasMax = false;
    }

    public int minSize()
    {
        if (!hasMin) {
            throw new RuntimeException("Attempting to read the minimum size of a " +
                    "list constraint without minimum size bound.");
        }

        return minSize;
    }

    public int maxSize()
    {
        if (!hasMax) {
            throw new RuntimeException("Attempting to read the maximum size of a " +
                    "list constraint without maximum size bound.");
        }

        return maxSize;
    }

    public int size()
    {
        if (minSize != maxSize) {
            throw new RuntimeException("Attempting to read the size of a " +
                    "list constraint without fixed size bound.");
        }

        return minSize;
    }

    @Override
    public PathEnabled readNode(String key)
    {
        return key.equals("item") ? itemConstraint : null;
    }
}
