package be_uclouvain_inl_sdload;

import be_uclouvain_inl_sdload.constraints.ChoiceConstraint;
import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.constraints.DictConstraint;
import be_uclouvain_inl_sdload.constraints.FloatRangeConstraint;
import be_uclouvain_inl_sdload.constraints.IntegerRangeConstraint;
import be_uclouvain_inl_sdload.constraints.ListConstraint;
import be_uclouvain_inl_sdload.constraints.UniformChoiceConstraint;
import be_uclouvain_inl_sdload.constraints.ValueConstraint;
import be_uclouvain_inl_sdload.constraints.WeightedChoiceConstraint;
import be_uclouvain_inl_sdload.constraints.WildcardConstraint;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTDict;
import be_uclouvain_inl_sdload.dlt.DLTList;
import be_uclouvain_inl_sdload.dlt.DLTValue;
import be_uclouvain_inl_sdload.generators.DLTGenerator;
import eu_norswap_utils.Caster;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 *
 */
public class DSL
{   
    public static interface Builder<T extends Constraint, Self extends Builder<T, Self>>
    {
        T get();

        default Self generator(DLTGenerator generator)
        {
            get().setGenerator(generator);
            return Caster.cast(this);
        }

        default Self generator(Supplier<?> supplier)
        {
            get().setGenerator(supplier);
            return Caster.cast(this);
        }

        default Self generatorUsingSelf(Function<T, DLTGenerator> supplier)
        {
            T constraint = get();
            get().setGenerator(supplier.apply(constraint));
            return Caster.cast(this);
        }

        default Self generator(BiFunction<T, DLT, DLT> generator)
        {
            T constraint = get();

            if (constraint == null) {
                throw new RuntimeException(
                    "Trying to set a generator referencing its constraint,  " +
                    "while that constrain has not yet been committed.");
            }

            get().setGenerator(parent -> generator.apply(constraint, parent));
            return Caster.cast(this);
        }

        default Self doBeforeGenerating(Runnable action)
        {
            DLTGenerator generator = get().generator();
            get().setGenerator(parent -> { action.run(); return generator.generate(parent); });
            return Caster.cast(this);
        }

        default Self extendGenerator(Function<DLT, DLT> transform)
        {
            DLTGenerator generator = get().generator();
            get().setGenerator(parent -> transform.apply(generator.generate(parent)));
            return Caster.cast(this);
        }

        default Self extendGenerator(BiFunction<DLT, DLT, DLT> transform)
        {
            DLTGenerator generator = get().generator();
            get().setGenerator(parent -> transform.apply(generator.generate(parent), parent));
            return Caster.cast(this);
        }

        default Self transform(Function<Self, Self> func)
        {
            return func.apply(Caster.cast(this));
        }

        default Self perform(Consumer<Self> consumer)
        {
            Self self = Caster.cast(this);
            consumer.accept(self);
            return self;
        }
    }

    public static class DefaultBuilder<T extends Constraint>
    implements Builder<T, DefaultBuilder<T>>
    {
        private T constraint;

        public DefaultBuilder(T constraint)
        {
            this.constraint = constraint;
        }

        @Override
        public T get()
        {
            return constraint;
        }
    }

    public static class ListBuilder implements Builder<ListConstraint, ListBuilder>
    {
        private final ListConstraint constraint;

        ListBuilder(ListConstraint constraint)
        {
            this.constraint = constraint;
        }

        @Override
        public ListConstraint get()
        {
            return constraint;
        }

        public ListBuilder min(int x)
        {
            get().setMinSize(x);

            if (get().hasMaxBound() && get().generator() == null)
            {
                get().setGeneratorFromBounds();
            }

            return this;
        }

        public ListBuilder max(int x)
        {
            get().setMaxSize(x);

            if (get().hasMinBound() && get().generator() == null)
            {
                get().setGeneratorFromBounds();
            }

            return this;
        }

        public ListBuilder size(int x)
        {
            get().setSize(x);

            if (get().generator() == null)
            {
                get().setGeneratorFromBounds();
            }

            return this;
        }

        public ListBuilder listGenerator(int minSize, int maxSize)
        {
            get().setGenerator(minSize, maxSize);
            return this;
        }

        public ListBuilder listGenerator(int size)
        {
            return listGenerator(size, size);
        }

        public ListBuilder listGenerator(Supplier<Integer> sizeSupplier)
        {
            get().setGeneratorFrom(sizeSupplier);
            return this;
        }
    }

    public static class ValueBuilder implements Builder<ValueConstraint, ValueBuilder>
    {
        private final ValueConstraint constraint;

        public ValueBuilder(ValueConstraint constraint)
        {
            this.constraint = constraint;
        }

        @Override
        public ValueConstraint get()
        {
            return constraint;
        }
    }

    public static class WildcardBuilder implements Builder<WildcardConstraint, WildcardBuilder>
    {
        private final WildcardConstraint constraint;

        public WildcardBuilder(WildcardConstraint constraint)
        {
            this.constraint = constraint;
        }

        @Override
        public WildcardConstraint get()
        {
            return constraint;
        }
    }

    public static class ChoiceBuilder implements Builder<ChoiceConstraint, ChoiceBuilder>
    {
        private final ChoiceConstraint constraint;

        public ChoiceBuilder(ChoiceConstraint constraint)
        {
            this.constraint = constraint;
        }

        @Override
        public ChoiceConstraint get()
        {
            return constraint;
        }
    }

    public static class WeightedChoiceBuilder
    implements Builder<WeightedChoiceConstraint, WeightedChoiceBuilder>
    {
        private WeightedChoiceConstraint constraint;
        private List<Constraint> alternatives;
        private List<Double> weights;

        public WeightedChoiceBuilder()
        {
            this.weights = new ArrayList<>();
            this.alternatives = new ArrayList<>();
        }

        @Override
        public WeightedChoiceConstraint get()
        {
            if (constraint == null) {
                throw new RuntimeException("Trying to retrieve a choice constraint that has not " +
                    "been built yet: you need to call 'commit()'.");
            }

            return constraint;
        }

        public WeightedChoiceBuilder or(Double weight, Constraint alternative)
        {
            if (constraint != null) {
                throw new RuntimeException("Trying to add an alternative to a choice constraint " +
                        "that has already been committed.");
            }

            alternatives.add(alternative);
            weights.add(weight);
            return this;
        }

        public WeightedChoiceBuilder or(Double weight, Builder<?, ?> builder)
        {
            return or(weight, builder.get());
        }

        public WeightedChoiceBuilder commit()
        {
            this.constraint = new WeightedChoiceConstraint(alternatives, weights);
            return this;
        }
    }

    public static class DictBuilder implements Builder<DictConstraint, DictBuilder>
    {
        private final DictConstraint constraint;

        public DictBuilder(DictConstraint constraint)
        {
            this.constraint = constraint;
        }

        @Override
        public DictConstraint get()
        {
            return constraint;
        }

        public DictBuilder key(String key, Constraint subConstraint)
        {
            get().keyConstraints.put(key, subConstraint);
            return this;
        }

        public DictBuilder key(String key, Builder<?, ?> builder)
        {
            return key(key, builder.get());
        }

        public DictBuilder opt(String key, Constraint subConstraint)
        {
            get().optKeyConstraints.put(key, subConstraint);
            return this;
        }

        public DictBuilder opt(String key, Constraint subConstraint, double weight)
        {
            subConstraint.properties().put("weight", weight);
            return opt(key, subConstraint);
        }

        public DictBuilder opt(String key, Builder<?, ?> builder)
        {
            return opt(key, builder.get());
        }

        public DictBuilder global(Constraint globalConstraint)
        {
            get().globalConstraint = globalConstraint;
            return this;
        }

        public DictBuilder global(Builder<?, ?> builder)
        {
            return global(builder.get());
        }

        public DictBuilder orphan(Constraint orphanConstraint)
        {
            if (!get().allowOrphans) {
                throw new RuntimeException("Trying to add a constraint for orphaned keys, " +
                        "but orphaned keys are disallowed.");
            }

            get().orphanConstraint = orphanConstraint;
            return this;
        }

        public DictBuilder orphan(Builder<?, ?> builder)
        {
            return orphan(builder.get());
        }

        public DictBuilder forbidOrphans()
        {
            get().allowOrphans = false;
            return this;
        }

        public DictBuilder orphanKeys(Predicate<String> predicate)
        {
            get().orphanKeyPredicate = predicate;
            return this;
        }

        public DictBuilder timestamp(Supplier<Integer> timeSupplier)
        {
            extendGenerator(dict ->
            {
                Caster.<DLTDict>cast(dict).put("timestamp", new DLTValue(timeSupplier.get()));
                return dict;
            });

            return this;
        }
    }

    public static ListBuilder list(Constraint itemConstraint)
    {
        return new ListBuilder(new ListConstraint(itemConstraint));
    }

    public static ListBuilder list(Builder<?, ?> builder)
    {
        return list(builder.get());
    }

    public static ValueBuilder value(Object value)
    {
        return new ValueBuilder(new ValueConstraint(value));
    }

    public static WildcardBuilder wildcard(Class<?> klass)
    {
        return new WildcardBuilder(new WildcardConstraint(klass));
    }

    public static DefaultBuilder<IntegerRangeConstraint> integer(long min, long max)
    {
        return new DefaultBuilder<>(new IntegerRangeConstraint(min, max));
    }

    public static DefaultBuilder<IntegerRangeConstraint> integer()
    {
        return new DefaultBuilder<>(new IntegerRangeConstraint());
    }

    public static DefaultBuilder<FloatRangeConstraint> floating(double min, double max)
    {
        return new DefaultBuilder<>(new FloatRangeConstraint(min, max));
    }

    public static DefaultBuilder<FloatRangeConstraint> floating()
    {
        return new DefaultBuilder<>(new FloatRangeConstraint());
    }

    public static ChoiceBuilder anyOf(Constraint... constraints)
    {
        return new ChoiceBuilder(new UniformChoiceConstraint(Arrays.asList(constraints)));
    }

    public static WeightedChoiceBuilder choice()
    {
        return new WeightedChoiceBuilder();
    }

    public static ChoiceBuilder anyOf(Builder<?, ?>... builders)
    {
        return new ChoiceBuilder(new UniformChoiceConstraint(Arrays.stream(builders)
                .<Constraint>map(b -> b.get())
                .collect(Collectors.toList())));
    }

    public static DictBuilder dict()
    {
        return new DictBuilder(new DictConstraint());
    }


    public static <T extends Constraint> DefaultBuilder<T> builder(T constraint)
    {
        return new DefaultBuilder<>(constraint);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static DLTGenerator compose(DLTGenerator first, Function<DLT, DLT> transform)
    {
        return parent -> transform.apply(first.generate(parent));
    }

    public static DLTGenerator compose(DLTGenerator first, BiFunction<DLT, DLT, DLT> transform)
    {
        return parent -> transform.apply(first.generate(parent), parent);
    }

    public static <T> DLTGenerator compose(Supplier<T> first, Function<T, DLT> transform)
    {
        return parent -> transform.apply(first.get());
    }

    public static <T> DLTGenerator compose(Supplier<T> first, BiFunction<T, DLT, DLT> transform)
    {
        return parent -> transform.apply(first.get(), parent);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static DLTList dltList(Object... objects)
    {
        DLTList out = new DLTList();

        for (Object object : objects)
        {
            if (object instanceof DLT) {
                out.add((DLT) object);
            }
            else {
                out.add(new DLTValue(object));
            }
        }

        return out;
    }

    public static DLTDict dltDict(Object... keyAndValues)
    {
        DLTDict out = new DLTDict();

        for (int i = 0 ; i + 1 < keyAndValues.length ; i += 2)
        {
            try {
                String key = Caster.cast(keyAndValues[i]);
                Object object = keyAndValues[i + 1];
                out.put(key, object instanceof DLT
                        ? Caster.cast(object)
                        : new DLTValue(object));
            }
            catch (ClassCastException e) {
                throw new RuntimeException(
                    "Parameters of dltDict must be alternance of strings and DLTs.");
            }
        }

        return out;
    }

    public static DLTValue dltValue(Object object)
    {
        return new DLTValue(object);
    }
}
