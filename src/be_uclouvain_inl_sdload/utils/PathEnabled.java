package be_uclouvain_inl_sdload.utils;

import java.util.Arrays;

/**
 *
 */
public interface PathEnabled
{
    public static class Implementation
    {
        public static String separator(PathEnabled self)
        {
            return "/";
        }

        public static Object read(PathEnabled self, String path)
        {
            Cons<String> cons =
                    Cons.<String>META().from(Arrays.asList(path.split(self.separator())));
            return self.read(cons);
        }

        public static Object read(PathEnabled self, Cons<String> path)
        {
            if (path.isEmpty()) {
                return self;
            }

            if (path.size() == 1) {
                return readLeafNonNull(self, path.head());
            }

            return readNodeNonNull(self, path.head()).read(path.tail());
        }

        private static Object readLeafNonNull(PathEnabled self, String key)
        {
            Object out = self.readLeaf(key);

            if (out == null) {
                throw new RuntimeException(String.format(
                        "Invalid path component \"%s\" for item: %s",
                        key, self));
            }

            return out;
        }

        private static PathEnabled readNodeNonNull(PathEnabled self, String key)
        {
            PathEnabled out = self.readNode(key);

            if (out == null) {
                throw new RuntimeException(String.format(
                        "Invalid path component \"%s\" for item: %s",
                        key, self));
            }

            return out;
        }
    }

    default String separator()
    {
        return Implementation.separator(this);
    }

    default Object readLeaf(String key)
    {
        return readNode(key);
    }

    PathEnabled readNode(String key);

    default Object read(String path)
    {
        return Implementation.read(this, path);
    }

    default Object read(Cons<String> path)
    {
        return Implementation.read(this, path);
    }
}
