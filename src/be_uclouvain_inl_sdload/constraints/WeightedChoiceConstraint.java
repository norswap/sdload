package be_uclouvain_inl_sdload.constraints;

import eu_norswap_utils.probabilities.WeightedSupplier;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * A choice constraint associated to a generator that selects one of the
 * alternatives according to weights associated with each alternative. The selected
 * alternative's generator is then used to construct the resulting value.
 */
public class WeightedChoiceConstraint extends ChoiceConstraint
{
    private List<Constraint> alternatives;
    private List<Double> weights;

    /**
     * Return an unmodifiable list of the weights for each of the alternatives (given in the same
     * order).
     */
    public List<Double> weights()
    {
        return weights;
    }

    /**
     * See {@link #setAlternatives(java.util.List, java.util.List)}.
     */
    public WeightedChoiceConstraint(List<Constraint> alternatives, List<Double> weights)
    {
        setAlternatives(alternatives, weights);
    }

    /**
     * Sets this choice's alternatives, and sets a generator that selects one of the
     * alternatives according to the supplied weights (there should be as many weights as
     * alternatives). We then use the selected alternative's generator to construct the resulting
     * value.
     */
    public void setAlternatives(List<Constraint> alternatives, List<Double> weights)
    {
        this.alternatives = alternatives;
        this.weights = weights;
        updateGenerator();
    }

    /**
     * {@inheritDoc} ({@link WeightedChoiceConstraint} -> unmodifiable)
     */
    @Override
    public List<Constraint> alternatives()
    {
        return alternatives;
    }

    /**
     * Modify the weight associated with the given constraint, which must be one of this
     * constraint's alternatives.
     */
    public void setWeightForConstraint(Constraint constraint, double weight)
    {
        int index = alternatives.indexOf(constraint);

        if (index == -1) {
            throw new RuntimeException(
                "Choice constraint does not include subconstraint: " + constraint);
        }

        weights.set(index, weight);
    }

    /**
     * Like {@link #setWeightForConstraint(Constraint, double), but uses a path component
     * to select a sub-constraint.
     */
    public void setWeightForConstraint(String component, double weight)
    {
        setWeightForConstraint(readNode(component), weight);
    }

    /**
     * Update the generator to take into account changes in alternatives and/or weights.
     */
    public void updateGenerator()
    {
        final WeightedSupplier<Constraint> supplier =
            WeightedSupplier.create(alternatives, weights);

        setGenerator(parent ->
            supplier.get().generator().generate(parent));
    }
}
