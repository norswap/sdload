package eu_norswap_utils;

import eu_norswap_utils.vendor.MinLog;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

/**
 * Logs messages to "%TMP/algalon/logs/%DATE", where %TMP is your temp directory and %DATE
 * is the date when the log file is created (upon loading this class) in ISO 8601 format.
 * Encoding is UTF-8.
 *
 * Optionally it is possible to also direct log statements to STDOUT. This is enabled by default.
 */
public class Log
{
    public static boolean logToSTDOUT = true;
    private static File outputFile;
    private static PrintWriter printer;

    static
    {
        // Create log file destination directory.
        File logOutputDir = new File(System.getProperty("java.io.tmpdir") + "algalon/logs");
        logOutputDir.mkdirs();

        if (!logOutputDir.exists()) {
            throw new RuntimeException(
                "Could not create log files output directory: " + logOutputDir);
        }

        outputFile = new File(logOutputDir, Dates.toISO8601(new Date(), "GMT+1"));

        try {
            printer = new PrintWriter(outputFile, "UTF-8");
        }
        catch (IOException e) {
            throw new RuntimeException("Could not create log file: " + outputFile, e);
        }

        MinLog.setLogger(new Logger());
    }

    static class Logger extends MinLog.Logger
    {
        @Override
        protected void print(String message)
        {
            if (logToSTDOUT) {
                System.out.println(message);
            }

            printer.println(message);
        }
    }

    public static void warn(String format, Object... args)
    {
        MinLog.warn(String.format(format, args));
    }

    public static void info(String format, Object... args)
    {
        MinLog.info(String.format(format, args));
    }

    public static void trace(String format, Object... args)
    {
        MinLog.trace(String.format(format, args));
    }
}
