package be_uclouvain_inl_sdload.generators.networking;

import be_uclouvain_inl_sdload.Context;
import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.Settings;
import be_uclouvain_inl_sdload.utils.networking.Topology;

import java.util.function.Supplier;

/**
 * This generator is only used when we have defined an underlying topology.
 * It supplies a random port which is in use on the destination switch for the current command.
 */
public class TopoPortGenerator implements Supplier<Long>
{
    public static final TopoPortGenerator get = new TopoPortGenerator();

    @Override
    public Long get()
    {
        Topology topology = Settings.get().topology();
        Topology.Switch destination = topology.switchWith(Context.get().destination);

        if (destination == null) {
            throw new RuntimeException("Destination switch id exceeds the switch count: " +
                Context.get().destination);
        }

        return (long) Randomizer.nextInt(topology.graph().getNeighborCount(destination)) + 1;
    }
}
