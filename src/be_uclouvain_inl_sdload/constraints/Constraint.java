package be_uclouvain_inl_sdload.constraints;

import be_uclouvain_inl_sdload.CheckDiagnostic;
import be_uclouvain_inl_sdload.generators.DLTGenerator;
import be_uclouvain_inl_sdload.dlt.DLT;
import eu_norswap_utils.Caster;
import be_uclouvain_inl_sdload.utils.PathEnabled;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * A constraint is a predicate that a DLT of interest should satisfy. It may also
 * be associated to a {@link be_uclouvain_inl_sdload.generators.DLTGenerator} able to generate DLTs
 * that satisfy the constraint.
 */
public interface Constraint extends PathEnabled
{
    /**
     * Return a constraint by supplying a check function, with default behaviour as specified in
     * {@link be_uclouvain_inl_sdload.constraints.AbstractConstraint}.
     */
    public static Constraint make(Function<DLT, CheckDiagnostic> checkFunction)
    {
        return new AbstractConstraint()
        {
            @Override
            public CheckDiagnostic check(DLT dlt)
            {
                return checkFunction.apply(dlt);
            }

            @Override
            public PathEnabled readNode(String key)
            {
                return null;
            }
        };
    }

    /**
     * Return a diagnostic indicating that the passed DLT satisfies this constraint, or why not.
     */
    CheckDiagnostic check(DLT dlt);

    /**
     * Return a generator that generates DLTs satisfying this constraint, or null if this
     * constraint does not have an attached generator.
     */
    default DLTGenerator generator()
    {
        return null;
    }

    /**
     * A convenience method that calls the {@link #generator()}'s
     * {@link DLTGenerator#generate(DLT)} method.
     */
    default DLT generate(DLT parent)
    {
        return generator().generate(parent);
    }

    /**
     * Supply a custom generator for this constraint.
     */
    void setGenerator(DLTGenerator generator);

    /**
     * Supply a custom generator for this constraint, built from passed supplier.
     * See {@link DLTGenerator#from(Supplier)}.
     */
    default void setGenerator(Supplier<?> supplier)
    {
        setGenerator(DLTGenerator.from(supplier));
    }

    /**
     * Return a dictionary of user-defined properties on the constraint.
     */
    Map<String, Object> properties();

    /**
     * Returns the sub-constraint at the given path, or throw an exception if the path is invalid.
     */
    default Constraint sub(String path)
    {
        return Caster.cast(read(path));
    }
}
