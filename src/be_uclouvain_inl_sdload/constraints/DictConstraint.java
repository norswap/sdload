package be_uclouvain_inl_sdload.constraints;

import be_uclouvain_inl_sdload.CheckDiagnostic;
import be_uclouvain_inl_sdload.Holes;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTDict;
import be_uclouvain_inl_sdload.generators.DictGenerator;
import eu_norswap_utils.Caster;
import be_uclouvain_inl_sdload.utils.PathEnabled;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Predicate;

/**
 * A constraint for dictionary objects that allows specifying mandatory keys and optional keys; as
 * well as sub-constraints associated with each of those keys. One can also specify a constraint
 * that applies to all keys, or to all keys which were not mentioned explicitly ("orphan keys").
 *
 * Note that the order in which keys are inserted does matter, as the key's values will be
 * generated in that order by the built-in generator (currently only for mandatory keys).
 */
public class DictConstraint extends AbstractConstraint
{
    /**
     * A map from mandatory keys to their associated sub-constraint.
     */
    public final Map<String, Constraint> keyConstraints;

    /**
     * A map from mandatory keys to their associated sub-constraint.
     */
    public final Map<String, Constraint> optKeyConstraints;

    /**
     * A constraint that all values in the dictionary should satisfy.
     */
    public Constraint globalConstraint;

    /**
     * Whether to allow orphan keys: keys that are not mentioned as being mandatory or optional.
     */
    public boolean allowOrphans = true;

    /**
     * A constraint that all values associated to orphan keys should satisfy.
     */
    public Constraint orphanConstraint;

    /**
     * Predicate all orphan keys have to satisfy.
     */
    public Predicate<String> orphanKeyPredicate;

    /**
     * Build a new dictionary constraint with empty set of mandatory and optional keys.
     *
     * The constraint has a generator based on the set of mandatory keys. The generator
     * is backed by the set, so that new mandatory keys are automatically taken into account.
     * Other constraints, however, are not taken into account by the generator.
     */
    public DictConstraint()
    {
        this.keyConstraints = new LinkedHashMap<>();
        this.optKeyConstraints = new LinkedHashMap<>();
        setGenerator(new DictGenerator(this));
    }

    /**
     * Build a new dictionary constraint with a set of mandatory keys and an empty set of
     * optional keys.
     *
     * The constraint has a generator based on the set of mandatory keys. The generator
     * is backed by the set, so that new mandatory keys are automatically taken into account.
     * Other constraints, however, are not taken into account by the generator.
     */
    public DictConstraint(Map<String, Constraint> subConstraints)
    {
        this.keyConstraints = new LinkedHashMap<>(subConstraints);
        this.optKeyConstraints = new LinkedHashMap<>();
        setGenerator(new DictGenerator(this));
    }

    @Override
    public CheckDiagnostic check(DLT dlt)
    {
        if (!(dlt instanceof DLTDict)) {
            return CheckDiagnostic.failed("not a DLT dictionary");
        }

        DLTDict dict = Caster.cast(dlt);

        // Check that all the mandatory key-value pairs are present and satisfy their associated
        // constraint.

        for (Map.Entry<String, Constraint> e : keyConstraints.entrySet())
        {
            DLT value = dict.get(e.getKey());

            if (value == null) {
                return new CheckDiagnostic(e.getKey(), "missing subkey");
            }

            CheckDiagnostic diag = checkSubConstraint(e.getValue(), value);

            if (!diag.satisfied()) {
                return new CheckDiagnostic(e.getKey(), diag, "failed subkey constraint");
            }
        }

        // Check that all the optional key-value pairs that are present satisfy their
        // associated constraint.

        for (Map.Entry<String, Constraint> e : optKeyConstraints.entrySet())
        {
            DLT value = dict.get(e.getKey());

            if (value != null)
            {
                CheckDiagnostic diag = checkSubConstraint(e.getValue(), value);

                if (!diag.satisfied()) {
                    return new CheckDiagnostic(e.getKey(), diag, "failed opt. subkey constraint");
                }
            }
        }

        // optimization
        if  (!allowOrphans || globalConstraint != null || orphanConstraint != null)

        // iterate over all key-value pairs
        for (Map.Entry<String, DLT> e : dict.entrySet())
        {
            String key = e.getKey();

            // Check that all key-value pairs satisfy the global constraint.

            if (globalConstraint != null)
            {
                CheckDiagnostic diag = globalConstraint.check(e.getValue());

                if (!diag.satisfied()) {
                    return new CheckDiagnostic(key, diag, "failed global constraint");
                }
            }

            // optimization
            if (!allowOrphans || orphanConstraint != null || orphanKeyPredicate != null)

            if (!keyConstraints.containsKey(key)&& !optKeyConstraints.containsKey(key))
            {
                // Check that the key is not orphaned if that is forbidden.

                if (!allowOrphans) {
                    return new CheckDiagnostic(key, "orphaned key");
                }

                if (orphanKeyPredicate != null) {
                    if (!orphanKeyPredicate.test(key)) {
                        return new CheckDiagnostic(key, "failed orphaned key predicate");
                    }
                }

                // Check that this key's value respects the constraint for orphaned keys.

                if (orphanConstraint != null)
                {
                    CheckDiagnostic diag = orphanConstraint.check(e.getValue());

                    if (!diag.satisfied()) {
                        return new CheckDiagnostic(key, diag, "failed orphaned constraint");
                    }
                }
            }

        }

        return CheckDiagnostic.SATISFIED;
    }

    /**
     * Check if a constraint applies on a child value -- overridable by subclasses.
     */
    protected CheckDiagnostic checkSubConstraint(Constraint constraint, DLT value)
    {
        return !(constraint instanceof ValueConstraint) && Holes.isHole(value)
                ? CheckDiagnostic.SATISFIED
                : constraint.check(value);
    }

    /**
     * Return the sub-constraint associated to the given key, whether it is mandatory or
     * optional; or null if there is no constraint associated with the given key.
     */
    public Constraint constraintForKey(String key)
    {
        Constraint c;
        if ((c = keyConstraints.get(key)) != null) {
            return c;
        }
        return optKeyConstraints.get(key);
    }

    @Override
    public String toString()
    {
        String keys    = keyConstraints.isEmpty()    ? "" : "keys="     + keyConstraints;
        String opt     = optKeyConstraints.isEmpty() ? "" : ", opt="    + optKeyConstraints;
        String global  = globalConstraint  == null   ? "" : ", global=" + globalConstraint;
        String orphan  = orphanConstraint  == null   ? "" : ", orphan=" + orphanConstraint;

        return String.format("dict{%s%s%s%s}", keys, opt, global, orphan);
    }

    @Override
    public Constraint readNode(String key)
    {
        return constraintForKey(key);
    }
}