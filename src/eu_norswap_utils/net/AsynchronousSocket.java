package eu_norswap_utils.net;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * This class wraps an {@link java.nio.channels.AsynchronousSocketChannel} and associates it with
 * two {@link ByteBuffer}, one for reading and one for writing. The user never has to manage the
 * read buffer's limit, which only encompasses received data, nor the write buffer's limit, which
 * is set to the capacity.
 *
 * The class supplies the {@link #readAppend(int, Runnable)} and {@link #readOver(int, Runnable)}
 * methods to read a number of bytes into the buffer. They take care of looping the read request
 * until all the required bytes haven been read.
 *
 * By default, the socket can read more byte than requested (to improve performance). Those
 * supplemental bytes are called "overflow bytes". It is possible to disable overflow. Overflow
 * bytes are not visible to the user (they live in the buffer between its limit and its capacity),
 * but the socket keeps track of them and uses them in subsequent {@link #readAppend(int, Runnable)}
 * calls.
 *
 * It is possible to have multiple sockets sharing the same channel, but concurrent reads are
 * not supported. When initiating a read on a socket, there should be no overflow bytes in any
 * socket sharing the same channel. And of course, no read operation should be ongoing,
 * meaning you need to use the completion handlers to sequence your sockets.
 *
 * For writing, use the {@link #write(Runnable)} method. It takes care of looping the
 * write request until all bytes have been written. Concurrent writes are not supported.
 * Use the completion handler to sequence your writes.
 *
 * All handlers can either be called immediately or asynchronously.
 */
public class AsynchronousSocket
{
    @FunctionalInterface
    public static interface FailureHandler
    {
        public void handle(Throwable exc);
    }

    public final AsynchronousSocketChannel chan;
    public final ByteBuffer rbuffer;
    public final ByteBuffer wbuffer;

    private FailureHandler failureHandler;
    private boolean allowOverflow = true;
    private int overflow = 0;

    /**
     * Creates a new "asynchronous socket" wrapping the given channel and backed by a read buffer
     * and a write buffer of the given capacities. Upon failure of read or write operations,
     * the supplied failure handler is invoked.
     */
    public AsynchronousSocket(
            int rcapacity,
            int wcapacity,
            AsynchronousSocketChannel chan,
            FailureHandler failureHandler)
    {
        this.rbuffer = ByteBuffer.allocate(rcapacity);
        this.wbuffer = ByteBuffer.allocate(wcapacity);
        this.chan = chan;
        this.failureHandler = failureHandler;

        rbuffer.limit(0);
    }

    /**
     * Allow this socket to read more than required (enabled by default).
     */
    public void allowOverflow()
    {
        allowOverflow = true;
    }

    /**
     * Forbid this socket to read more than required. There must not be any overflow bytes
     * when this method is called. You can ensure this by using the {@link #drainOverflow()} method.
     */
    public void forbidOverflow()
    {
        assert overflow == 0;
        allowOverflow = false;
    }

    /**
     * Extends the buffer's limit to cover the overflow bytes, and return the number of such bytes.
     * As a result, the overflow byte count is cleared.
     */
    public int drainOverflow()
    {
        rbuffer.limit(rbuffer.limit() + overflow);
        int oldOverflow = overflow;
        overflow = 0;
        return oldOverflow;
    }

    /**
     * Read n byte in at index 0 in the buffer, overwriting any content that might have been there.
     * Those bytes should fit in the buffer.
     * Invoke {@code successHandler) or {@link #failureHandler} depending on the result.
     * The buffer's position is set to 0, the limit to n.
     * Can only be called if there are no overflow bytes.
     */
    public void readOver(int n, Runnable successHandler)
    {
        System.out.println(overflow);
        assert overflow == 0;
        chanRead(0, n, 0, successHandler);
    }

    /**
     * Read n byte at the current buffer's limit, which is consequently pushed back by n.
     * Those bytes should fit in the buffer).
     * Invoke {@code successHandler) or {@link #failureHandler} depending on the result.
     * The buffer's position is unchanged.
     */
    public void readAppend(int n, Runnable successHandler)
    {
        if (overflow >= n)
        {
            overflow -= n;
            rbuffer.limit(rbuffer.limit() + n);
            successHandler.run();
            return;
        }

        n -= overflow;
        chanRead(rbuffer.limit() + overflow, n, rbuffer.position(), successHandler);
    }

    /**
     * Writes {@code toReceive} bytes at {@code index}. Those bytes should fit in the buffer.
     *
     * Once the requested bytes have been received, sets the buffer's position to
     * {@code endPosition}  and its limit so that it covers requested bytes, but not
     * any overflow bytes that may have been read. The requested end position should be lower
     * than this limit.
     */
    private void chanRead(int index, int toReceive, int endPosition, Runnable successHandler)
    {
        int limit = index + toReceive;
        assert endPosition <= limit && limit <= rbuffer.capacity();

        rbuffer.limit(allowOverflow ? rbuffer.capacity() : limit);
        rbuffer.position(index);

        chan.read(rbuffer, null, new CompletionHandler<Integer, Object>()
        {
            @Override
            public void completed(Integer received, Object att)
            {
                if (rbuffer.position() < limit) {
                    // Recursive completion handler invocation.
                    chan.read(rbuffer, null, this);
                    return;
                }

                overflow = rbuffer.position() - limit;
                rbuffer.limit(limit);
                rbuffer.position(endPosition);
                successHandler.run();
            }

            @Override
            public void failed(Throwable exc, Object attachment)
            {
                failureHandler.handle(exc);
            }
        });
    }

    /**
     * Writes all the bytes in the write buffer that precede the buffer's position.
     * Invoke {@code successHandler) or {@link #failureHandler} depending on the result.
     */
    public void write(Runnable successHandler)
    {
        wbuffer.flip();

        chan.write(wbuffer, null, new CompletionHandler<Integer, Object>()
        {
            @Override
            public void completed(Integer result, Object attachment)
            {
                if (wbuffer.position() < wbuffer.limit()) {
                    // Recursive completion handler invocation.
                    chan.write(wbuffer, null, this);
                    return;
                }

                wbuffer.clear();
                successHandler.run();
            }

            @Override
            public void failed(Throwable exc, Object attachment)
            {
                failureHandler.handle(exc);
            }
        });
    }
}