package be_uclouvain_inl_sdload.dlt;

import eu_norswap_utils.Caster;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Utilities used to convert between DLTs and hierarchies made of vanilla Java list, maps and
 * other objects.
 */
public class DLTs
{
    public static DLT from(Object hierarchy)
    {
        /**/ if (hierarchy instanceof List<?>) {
            return fromList(Caster.cast(hierarchy));
        }
        else if (hierarchy instanceof Map<?, ?>) {
            return fromDict(Caster.cast(hierarchy));
        }
        else {
            return fromValue(hierarchy);
        }
    }

    public static DLTList fromList(List<?> list)
    {
        DLTList out = new DLTList(list.size());
        list.forEach(o -> out.add(from(o)));
        return out;
    }

    public static DLTDict fromDict(Map<String, ?> dict)
    {
        DLTDict out = new DLTDict(dict.size());
        dict.entrySet().forEach(e -> out.put(e.getKey(), from(e.getValue())));
        return out;
    }

    public static DLTValue fromValue(Object value)
    {
        return new DLTValue(value);
    }

    public static Object toVanillaHierarchy(DLT dlt)
    {
        /**/ if (dlt instanceof DLTList)
        {
            DLTList list = Caster.cast(dlt);
            List<Object> out = new ArrayList<>(list.size());
            list.forEach(o -> out.add(toVanillaHierarchy(o)));
            return out;
        }
        else if (dlt instanceof DLTDict)
        {
            DLTDict dict = Caster.cast(dlt);
            LinkedHashMap<String, Object> out = new LinkedHashMap<>(dict.size());
            dict.entrySet().forEach(e ->
                    out.put(e.getKey(), toVanillaHierarchy(e.getValue())));
            return out;
        }
        else if (dlt instanceof DLTValue)
        {
            return Caster.<DLTValue>cast(dlt).value;
        }

        throw new RuntimeException("Unknown dlt type: " + dlt.getClass());
    }
}
