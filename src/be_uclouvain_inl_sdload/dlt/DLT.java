package be_uclouvain_inl_sdload.dlt;

/**
 * A Dictionary-List Tree (DLT) is one of:
 * <ul>
 * <li>(DLTList)  a list whose items are DLTs</li>
 * <li>(DLTDict)  a dictionary whose keys are strings and whose values are DLTs</li>
 * <li>(DLTValue) a wrapper for any Java object</li>
 * </ul>
 *
 * Each DLT keeps a reference to its parent. The root's parent is null.
 * The descendant of a DLT can be accessed using a DLT path ({@link DLTPath}).
 */
public interface DLT extends Cloneable
{
    /**
     * Return the parent of this DLT.
     */
    DLT parent();

    /**
     * Return the relative path (as a single component) from the parent to this DLT, or null
     * if this DLT does not have a parent.
     */
    DLTPath.Component parentComponent();

    /**
     * Change the parent of this DLT.
     */
    void setParent(DLT parent, DLTPath.Component parentComponent);

    /**
     * Convert the string into a DLT path, then acts like {@link #getAtPath(DLTPath)}.
     * Throws an exception if the supplied string cannot be converted to a DLT path.
     */
    default Object getAtPath(String path)
    {
        return getAtPath(DLTPath.from(path));
    }

    /**
     * Return the descendant of the DLT at the given path (relative to this DLT).
     * Throws an exception if there is no descendant with the given path.
     */
    Object getAtPath(DLTPath path);

    /**
     * Makes a deep copy of this DLT.
     */
    Object clone();
}
