package be_uclouvain_inl_sdload.dlt;

import eu_norswap_utils.Caster;

/**
 * A DLT that wraps a java Object.
 */
public class DLTValue implements DLT
{
    private DLT parent;
    private DLTPath.Component parentComponent;

    public Object value;

    public DLTValue(Object value)
    {
        this.value = value;
    }

    @Override
    public DLT parent()
    {
        return parent;
    }

    @Override
    public DLTPath.Component parentComponent() { return parentComponent; }

    @Override
    public void setParent(DLT parent, DLTPath.Component parentComponent)
    {
        this.parent = parent;
        this.parentComponent = parentComponent;
    }

    @Override
    public Object getAtPath(DLTPath path)
    {
        if (path.isEmpty())
        {
            return this;
        }

        if (path.head() instanceof DLTPath.Parent)
        {
            return parent.getAtPath(path.tail());
        }

        throw new RuntimeException("Wrong path component for DLT value: "
                + path.head().getClass());
    }

    @Override
    public String toString()
    {
        if (value instanceof String) {
            return "\"" + value + "\""; // for compatibility with JSON
        }

        return value.toString();
    }

    @Override
    public DLTValue clone()
    {
        try {
            DLTValue out = Caster.cast(super.clone());
            out.parent = parent;
            return out;
        }
        catch (CloneNotSupportedException e) {
            // this will never happen
            throw new RuntimeException(e);
        }
    }
}
