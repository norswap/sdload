package be_uclouvain_inl_sdload.generators;

import eu_norswap_utils.probabilities.Randomizer;
import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTDict;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Generate a dictionary with a subset of the keys it was supplied with at instantiation time.
 * Each key is also paired with a constraint that its associated value must satisfy.
 *
 * If the constraint paired with a key features a "weight" attribute (should be an integer), then
 * the probability that the key will be selected in the subset will be scaled by its weight
 * relative to the weight of the other keys. The default weight is 1.
 */
public class SubDictionaryGenerator implements DLTGenerator
{
    private Supplier<Number> sizeSupplier;
    private Map<String, Constraint> subConstraints;

    public SubDictionaryGenerator(
        Supplier<Number> sizeSupplier,
        Map<String, Constraint> subConstraints)
    {
        this.sizeSupplier = sizeSupplier;
        this.subConstraints = subConstraints;
    }

    @Override
    public DLT generate(DLT parent)
    {
        int size = sizeSupplier.get().intValue();
        DLTDict out = new DLTDict();

        /**
         * We want {@code size} keys, but we cannot have duplicate keys.
         * Keys can be weighted so that some are more frequent than other.
         *
         * Balance all of this by assigning a new weight to each key, which is the product of
         * its original weight and a random factor, then sort the keys in decreasing order of
         * new weight and select {@code size} first keys from that list.
         */

        List<Map.Entry<String, Constraint>> ordered =
          new ArrayList<>(subConstraints.entrySet());

        Map<Constraint, Integer> weights = new HashMap<>(ordered.size());

        for (Map.Entry<String, Constraint> e : ordered)
        {
            Constraint c = e.getValue();
            Object weight = c.properties().getOrDefault("weight", 1);

            weights.put(c, Randomizer.nextInt(10) * (int) weight);
        }

        ordered.sort((x, y) -> weights.get(x.getValue()) - weights.get(y.getValue()));

        for (int i = 0 ; i < size ; ++i)
        {
            Map.Entry<String, Constraint> e = ordered.get(i);
            out.put(e.getKey(), e.getValue().generate(out));
        }

        return out;
    }
}
