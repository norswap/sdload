#!/bin/bash

# Use this script to ensure that maven does not use the Apple-supplied JDK on
# Mac OSX, but rather a newly installed JDK (which should version 1.8+).

export JAVA_HOME=$(/usr/libexec/java_home)
