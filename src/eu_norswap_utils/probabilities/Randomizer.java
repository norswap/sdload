package eu_norswap_utils.probabilities;

import eu_norswap_utils.Caster;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.RandomAccess;

/**
 * Utilities related to randomness.
 *
 * Note when generating longs: the comment from {@link java.util.Random#nextLong()} applies:
 * "Because class Random uses a seed with only 48 bits, this algorithm will not return all
 * possible long values."
 */
public class Randomizer
{
    private static final SecureRandom random = new SecureRandom();

    public static Random random()
    {
        return random;
    }

    /**
     * See {@link Random#nextInt(int)}.
     */
    public static int nextInt(int bound)
    {
        return random.nextInt(bound);
    }

    /**
     * Like {@link Random#nextInt(int)}, but for longs.
     */
    public static long nextLong(long bound)
    {
        long rand, val;

        do {
            rand = random.nextLong();
            val = rand % bound;
        } while (rand - val + (bound - 1) < 0);

        return val;
    }

    /**
     * Return an int between min (inclusive) and max (exclusive).
     */
    public static int between(int min, int max)
    {
        return min + nextInt(max - min);
    }

    /**
     * Return a long between min (inclusive) and max (exclusive).
     */
    public static long between(long min, long max)
    {
        return min + nextLong(max - min);
    }

    /**
     * Return an int between min and max (both inclusive).
     */
    public static int betweenClosed(int min, int max)
    {
        return min + nextInt(max - min + 1);
    }

    /**
     * Return a long between min and max (both inclusive).
     */
    public static long betweenClosed(long min, long max)
    {
        return min + nextLong(max - min + 1);
    }

    /**
     * Return a double between min and max (both inclusive).
     */
    public static double between(double min, double max)
    {
        return min + (max - min) * random.nextDouble();
    }

    /**
     * Returns a random element from {@code collection}. O(1) if {@code collection} implements
     * {@link java.util.RandomAccess}, O(n) otherwise.
     */
    public static <T> T randomElem(Collection<T> collection)
    {
        int num = random.nextInt(collection.size());

        if (collection instanceof List<?> && collection instanceof RandomAccess)
        {
            return Caster.<List<T>>cast(collection).get(num);
        }

        int i = 0;
        for (T t : collection) {
            if (i++ == num) {
                return t;
            }
        }
        throw new RuntimeException("not supposed to happen");
    }

    /**
     * Returns a random element from {@code collection}. O(1).
     */
    public static <T, L extends List<T> & RandomAccess> T randomElem(L list)
    {
        return list.get(random.nextInt(list.size()));
    }

    /**
     * Return a copy of {@code items} sorted in random order, weighted by {@code weights}.
     *
     * The probability of an element getting into the first position is equal by its weight
     * divided by the sum of all weights, and the probability of getting into a position other
     * than the first is equal to the probability of getting in the first position when all
     * elements in prior positions have had their weights set to 0.
     *
     * Of course, under this definition a weight of 0 in the original weight list cannot work,
     * so weights of 0 are treated like infinitely small non-zero weights.
     */
    public static <T> List<T> randomOrder(List<T> items, List<Double> weights)
    {
        List<Double> newWeights = new ArrayList<>(weights);
        List<T>      out        = new ArrayList<>(items);

        double weightSum = newWeights.stream().mapToDouble(Double::doubleValue).sum();

        for (int j = 0 ; j < out.size() ; ++j)
        {
            double rand  = between(0, weightSum);
            double total = 0;

            for (int i = j ; i < out.size() ; ++i)
            {
                total += weights.get(i);

                if (rand <= total)
                {
                    weightSum -= weights.get(i);
                    Collections.swap(out, i, j);
                    Collections.swap(weights, i, j);
                    break;
                }
            }
        }

        return out;
    }
}