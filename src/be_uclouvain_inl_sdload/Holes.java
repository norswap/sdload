package be_uclouvain_inl_sdload;

import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTValue;
import eu_norswap_utils.Caster;

/**
 * A hole (represented by a dollar sign: "$") can be used inside templates to signify that the
 * value at the position where the hole appears should be generated.
 */
public class Holes
{
    public static boolean isHole(DLT dlt)
    {
        return dlt instanceof DLTValue && "$".equals(Caster.<DLTValue>cast(dlt).value);
    }
}
