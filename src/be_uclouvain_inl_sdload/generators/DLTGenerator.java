package be_uclouvain_inl_sdload.generators;

import be_uclouvain_inl_sdload.dlt.DLT;
import be_uclouvain_inl_sdload.dlt.DLTValue;

import java.util.function.Supplier;

/**
 * Interface for generating DLTs from a constraint.
 */
public interface DLTGenerator
{
    /**
     * Generate a DLT from a constraint and from its parent DLT. Passing down the parent is
     * required in order to be able to take into account the already generated part of the
     * DLT in the generation.
     */
    public DLT generate(DLT parent);

    /**
     * Turns a supplier into a DLT generator that returns a DLT value wrapping the
     * result of the supplier
     */
    public static DLTGenerator from(Supplier<?> supplier)
    {
        return (parent) -> new DLTValue(supplier.get());
    }

    /**
     * Turns a value into a DLT generator that always return a DLT value wrapping the original
     * value.
     */
    public static DLTGenerator from(Object object)
    {
        return (parent) -> new DLTValue(object);
    }
}
