package eu_norswap_utils.probabilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Classes implementing this interface have an associated weight.
 * This can be use for instance to encode a probability.
 *
 * A normalized set of weight is a set set of weights whose sum is 1.
 */
public interface Weighted
{
    /**
     * Normalizes a set of weights, returning a list containing the normalized weights, in the
     * original collection iteration's order.
     */
    static List<Double> normalize(Collection<Double> collection)
    {
        List<Double> out = new ArrayList<>(collection.size());
        Double total = collection.stream().mapToDouble(Double::doubleValue).sum();

        for (Double d : collection) {
            out.add(d / total);
        }

        return out;
    }

    /**
     * Normalizes a list of weights in place. The iterator of the list must support the optional
     * `set()` operation.
     */
    static void normalizeInPlace(List<Double> list)
    {
        Double total = list.stream().mapToDouble(Double::doubleValue).sum();

        for (ListIterator<Double> iter = list.listIterator() ; iter.hasNext() ;)
        {
            iter.set(iter.next() / total);
        }
    }

    /**
     * Normalizes the weights of a set of weighted objects in place.
     */
    static void normalizeWeights(Collection<Weighted> collection)
    {
        Double total = collection.stream().mapToDouble(Weighted::weight).sum();

        for (Weighted w : collection) {
            w.setWeight(w.weight() / total);
        }
    }

    Double weight();

    void setWeight(Double weight);
}
