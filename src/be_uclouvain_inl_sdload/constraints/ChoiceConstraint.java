package be_uclouvain_inl_sdload.constraints;

import be_uclouvain_inl_sdload.CheckDiagnostic;
import be_uclouvain_inl_sdload.dlt.DLT;
import eu_norswap_utils.Caster;
import be_uclouvain_inl_sdload.utils.PathEnabled;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A constraint that matches DLTs that satisfies any of its sub-constraints (aka alternatives).
 */
public abstract class ChoiceConstraint extends AbstractConstraint
{
    /**
     * Return an this choice's alternatives. May or may not be modifiable depending on the
     * implementation.
     */
    public abstract List<Constraint> alternatives();

    @Override
    public CheckDiagnostic check(DLT dlt)
    {
        List<CheckDiagnostic> diags = new ArrayList<>(alternatives().size());

        for (Constraint c : alternatives())
        {
            CheckDiagnostic diag = c.check(dlt);

            if (diag.satisfied()) {
                return CheckDiagnostic.SATISFIED;
            }

            diags.add(diag);
        }

        return new CheckDiagnostic(diags, "no matching alternative");
    }

    @Override
    public String toString()
    {
        return "choice" + alternatives();
    }

    @Override
    public Constraint readNode(String key)
    {
        try {
            int index = Integer.parseInt(key);
            return alternatives().get(index);
        }
        catch (NumberFormatException e) {}

        String[] components = key.split("=");

        if (components.length > 1)
        {
            String rightHand = String.join("=",Arrays.copyOfRange(components, 1,components.length));

            for (Constraint c : alternatives()) {
                if (c instanceof DictConstraint)
                {
                    Constraint sub = Caster.<DictConstraint>cast(c).constraintForKey(components[0]);
                    if (sub != null && sub.toString().equals(rightHand)) {
                        return c;
                    }
                }
            }
        }

        return null;
    }
}
