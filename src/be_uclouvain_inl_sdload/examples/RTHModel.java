package be_uclouvain_inl_sdload.examples;

import be_uclouvain_inl_sdload.ApplicationModel;
import be_uclouvain_inl_sdload.SDLoad;
import be_uclouvain_inl_sdload.Settings;
import be_uclouvain_inl_sdload.constraints.Constraint;
import be_uclouvain_inl_sdload.dlt.DLTDict;
import be_uclouvain_inl_sdload.utils.ParameterHolder;
import be_uclouvain_inl_sdload.utils.networking.JellyfishTopoFactory;
import be_uclouvain_inl_sdload.utils.networking.StdRandom;
import be_uclouvain_inl_sdload.utils.networking.Topology;
import com.beust.jcommander.Parameter;
import eu_norswap_utils.Strings;
import be_uclouvain_inl_sdload.utils.networking.Addresses;
import be_uclouvain_inl_sdload.utils.networking.PrefixedMaker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static be_uclouvain_inl_sdload.utils.networking.Topology.Switch;
import static be_uclouvain_inl_sdload.DSL.*;

/**
 * Model to generate input for Routing Tree Heuristic (RTH) algorithm implementation.
 * This algorithm automatically updates iBGP/SDN policies to obtain the desired forwarding graph,
 * starting from the current forwarding graph.
 *
 * In particular, we consider the case of VM migration in a data center and how the migration of
 * a VM affects the forwarding graph.
 *
 * The output is a sequence of forwarding graphs. Each graph is represented by a map from
 * VM addresses to a map from switches to a next hop (represented by switch IDs) to reach the VM.
 *
 * We identify each end host on which a VM can hosted by a unique sequential ID and assume that the
 * last-hop before this end host is the local switch with the same ID.
 */
public class RTHModel implements ApplicationModel
{
    public Constraint constraint;

    // ---------------------------------------------------------------------------------------------
    // options

    public static class Options implements ParameterHolder
    {
        /**
         * The number of VMs to use.
         */
        @Parameter(names = "-nVMs")
        public int nVMs = 1;

        /**
         * The number of VM to migrate between each successive forwarding graph.
         */
        @Parameter(names = "-nMovingVMs")
        public int nMovingVMs = 2;

        /**
         * The number of forwarding graphs to generate.
         */
        @Parameter(names = "-nForwardingGraphs")
        public int nForwardingGraphs = 2;

        @Override
        public void configure()
        {
            if (nVMs < nMovingVMs) {
                nVMs = nMovingVMs;
            }
        }
    }

    public static final Options OPTIONS = new Options();

    // ---------------------------------------------------------------------------------------------
    // constraint definition

    @Override
    public void buildConstraint()
    {
        Settings s = Settings.get();

        Map<Integer, Integer> dests2switches = new HashMap<Integer, Integer>();

        Map<Integer, Integer> dests2blacklistedswitches = new HashMap<Integer, Integer>();

        PrefixedMaker foreignIPs = new PrefixedMaker.IPv4(12, 0);

        this.constraint =
        list(
            dict()
                .orphanKeys(Addresses.IS_IPV4)
                .orphan(dict()
                    // The key must be a switch identifier.
                    .orphanKeys(key -> {
                        Integer num = Strings.parseInt(key);
                        return num != null && num >= 0 && num < s.nSwitches;
                    })

                    .orphan(list(wildcard(String.class)))
                )
                .generator(parent ->
                {
                    //System.out.println("\nGenerating new forwarding graph");

                    DLTDict dict = new DLTDict();

                    // remove N random values to the dests2switches map
                    for (int i = 0; i < OPTIONS.nMovingVMs; i++) {
                        Integer mdest = new Integer(StdRandom.uniform(OPTIONS.nVMs));
                        //System.out.println("Moving destination " + mdest);
                        if (dests2switches.containsKey(mdest)) {
                            dests2blacklistedswitches.put(mdest, dests2switches.get(mdest));
                            dests2switches.remove(mdest);
                        }
                    }

                    // Weighter.randomlyWeightGraph(1, 10);

                    // generate next-hop dictionary for the missing destinations
                    for (int i = 0; i < OPTIONS.nVMs; i++) {
                        DLTDict switchsToNextHops = new DLTDict();

                        Integer dst_id = new Integer(i);

                        int dst_switch = -1;

                        if (dests2switches.containsKey(dst_id) && !dests2blacklistedswitches.containsKey(dst_id))
                        {
                            dst_switch = dests2switches.get(dst_id);
                        }

                        else
                        {
                            Integer blacklisted = dests2blacklistedswitches.get(dst_id);
                            do {
                                dst_switch = StdRandom.uniform(s.nSwitches);
                            } while (blacklisted != null && dst_switch == blacklisted);
                        }

                        dests2switches.put(dst_id,dst_switch);

                        //System.out.println("Destination " + dst_id + " attached to switch " + dst_switch);

                        for (int j = 0; j < s.nSwitches; j++) {
                            List<Switch> shortest = s.topology().shortestPath(j, dst_switch);

                            // Generate switch IDs for next hop.
                            String nextHop = String.valueOf(shortest.get(shortest.size() > 1 ? 1 : 0).id);

                            if (nextHop.equals(String.valueOf(j))){
                                nextHop = foreignIPs.make(i);
                            }

                            switchsToNextHops.put(String.valueOf(j), dltList(String.valueOf(nextHop)));
                        }

                        dict.put(foreignIPs.make(i), switchsToNextHops);
                    }
                    return dict;
                })
        ).listGenerator(OPTIONS.nForwardingGraphs)
        .get();
    }

    // ---------------------------------------------------------------------------------------------
    // model

    @Override
    public Constraint constraint()
    {
        return constraint;
    }

    @Override
    public ParameterHolder options()
    {
        return OPTIONS;
    }

    // ---------------------------------------------------------------------------------------------
    // main

    public static void main(String[] args)
    {
        OPTIONS.fillWithCmdLineParameters(args);
        Settings.get().fillWithCmdLineParameters(args);

        Settings s = Settings.get();
        s.setTopology(new Topology(JellyfishTopoFactory.generate(s.nSwitches, s.nPortsPerSwitch)));

        SDLoad.run(new RTHModel());
    }
}