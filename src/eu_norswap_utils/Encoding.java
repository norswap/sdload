package eu_norswap_utils;

import java.nio.charset.Charset;

public class Encoding
{
    public static final Charset ASCII = Charset.forName("ASCII");
    public static final Charset UTF_8 = Charset.forName("UTF-8");
}
