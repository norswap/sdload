package be_uclouvain_inl_sdload.utils;

import eu_norswap_utils.Caster;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.Stack;

/**
 * An abstract base class for immutable linked-list implementation using the cons
 * principle: a list is either the empty list or a (value,list) pair.
 *
 * For this to work well with inheritance, it is required to use the type of the subclass as
 * the value for the type parameter {@code Self}. Otherwise it would not be possible to enforce
 * that the tail of a subclass instance is itself a subclass instance.
 *
 * For a standard implementation of this abstract class, see {@link Cons}.
 */
public abstract class ConsList<T, Self extends ConsList<T, Self>> extends AbstractList<T>
{
    public static abstract class MetaConsList<T, Self extends ConsList<T, Self>>
    {
        public abstract Self cons(T head, Self tail);

        public abstract Self empty();

        public Self from(Iterable<T> collection)
        {
            Stack<T> reverse = new Stack<>();

            for (T item : collection) {
                reverse.push(item);
            }

            Self out = empty();

            while (!reverse.isEmpty()) {
                out = out.prepend(reverse.pop());
            }

            return out;
        }
    }

    public abstract MetaConsList<T, Self> meta();

    private final T head;
    private final Self tail;
    private final int size;

    /**
     * Empty list constructor; only use in subclasses: client classes should use the
     * {@code EMPTY ()} static method defined by the subclass instead.
     */
    protected ConsList()
    {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public ConsList(T head, Self tail)
    {
        this.head = head;
        this.tail = tail;
        this.size = tail.size() + 1;
    }

    @Override
    public T get(int index)
    {
        if (isEmpty()) {
            throw new IndexOutOfBoundsException();
        }

        if (index == 0) {
            return head;
        }

        return tail.get(index - 1);
    }

    @Override
    public int size()
    {
        return size;
    }

    @Override
    public Iterator<T> iterator()
    {
        return new Iterator<T>()
        {
            ConsList<T, ?> list = ConsList.this;

            @Override
            public boolean hasNext()
            {
                return !list.isEmpty();
            }

            @Override
            public T next()
            {
                T out = list.head;
                list = list.tail;
                return out;
            }
        };
    }

    /**
     * Return a new list whose head is {@code elem} and whose tail is this list.
     */
    public Self prepend(T elem)
    {
        return meta().cons(elem, Caster.cast(this));
    }

    public T head()
    {
        return head;
    }

    public Self tail()
    {
        return tail;
    }
}
