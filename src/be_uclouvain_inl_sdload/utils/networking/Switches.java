package be_uclouvain_inl_sdload.utils.networking;

/**
 * Utilities to work with switches identifiers.
 */
public class Switches
{
    /**
     * First 2 bytes of all switches IP address.
     */
    public static Integer[] SWITCH_IPV4_PREFIX = new Integer[]{10, 0};

    /**
     * First 4 bytes of all switches MAC address.
     */
    public static Integer[] SWITCH_MAC_PREFIX = new Integer[]{0, 0, 0, 0};

    /**
     * Maker of switch IPv4 addresses with {@link #SWITCH_IPV4_PREFIX}.
     */
    public static PrefixedMaker.IPv4 IPV4_MAKER = new PrefixedMaker.IPv4(SWITCH_IPV4_PREFIX);

    /**
     * Maker of switch MAC addresses with {@link #SWITCH_MAC_PREFIX}.
     */
    public static PrefixedMaker.MAC  MAC_MAKER  = new PrefixedMaker.MAC(SWITCH_MAC_PREFIX);
}
