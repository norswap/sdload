package eu_norswap_utils;

/**
 * Allows for terse unchecked casting.
 *
 * ---
 *
 * Now you can write:
 *
 *     Object x = "hello";
 *     boolean atEnd = "hey gorgeous, hello".endsWith(Caster.cast(x));n
 *
 * instead of:
 *
 *     Object x = "hello";
 *     @SuppressWarnings("unchecked")
 *     String y = (String) x;
 *     boolean atEnd = "hey gorgeous, hello".endsWith(y);
 *
 * So, it does two things: (1) automatically infer the destination type and (2)
 * avoid an intermediate variable declaration, which you need if you want to ignore
 * the resulting "unchecked cast" warning.
 *
 * The type inference is only available on Java 8. The definition of the `cast`
 * function is valid on Java 5 and higher however, and you still get benefit (2).
 *
 * In the cases where the type cannot be inferred, you need to specify it
 * explicitly:
 *
 *     Object x = "hello";
 *     boolean atEnd = Caster.<String>cast(x).endsWith("lo");
 */
public class Caster
{
    @SuppressWarnings("unchecked")
    public static <T> T cast(Object obj)
    {
        return (T) obj;
    }
}
